import React, { useState } from 'react';
import { View, Image } from 'react-native';
import { Text } from 'react-native-elements';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {
  AsyncStorageKeys,
  Colors,
  ModalTexts,
  ScreenDimensions,
} from '../../shared/constants';
import { useDispatch, useSelector } from 'react-redux';
import * as authActions from '../../store/actions/auth';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { login, setFcmToken } from '../../infrastructure/api/api';
import { toggleLoading } from '../../store/actions/loadingActions';
import LoadingSpinner from '../../components/loading/LoadingSpinner';
import ModalCard from '../../components/modal/Modal';
import { toggleModal } from '../../store/actions/modal';
import { Icon } from 'react-native-elements';
import { Formik } from 'formik';
import * as Yup from 'yup';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import FormInput from '../../components/form-input/FormInput';
import InputValidationCircle from '../../components/input-validation-circle/InputValidationCircle';
import CustomButton from '../../components/custom-button/CustomButton';
import { useTranslation } from 'react-i18next';

const LoginScreen = props => {
  const dispatch = useDispatch();
  const loading = useSelector(state => state.loading.loadingState);
  const [modalShown, setModalShown] = useState(false);
  const [hidePass, setHidePass] = useState(true);

  const { i18n, t } = useTranslation();

  const LoginSchema = Yup.object().shape({
    email: Yup.string()
      .email(t('common:email_invalid_error_login_register_screen'))
      .required(t('common:email_required_error_login_register_screen')),
    password: Yup.string()
      .min(8, t('common:password_short_error_login_register_screen'))
      .max(50, t('common:password_long_error_login_register_screen'))
      .required(t('common:password_required_error_login_register_screen')),
  });

  const setLanguage = code => {
    return i18n.changeLanguage(code);
  };

  const onSubmitPress = values => {
    dispatch(toggleLoading(true));

    login(values.email.toLowerCase(), values.password)
      .then(async response => {
        console.log('here');
        if (response?.data) {
          dispatch(toggleLoading(false));
          console.log('response.data', response.data);
          let fcmToken = await AsyncStorage.getItem('fcmToken');
          dispatch(authActions.setCurrentUser(response?.data));
          dispatch(authActions.updateUserPoints(response?.data.points));
          await AsyncStorage.setItem(
            AsyncStorageKeys.USER_KEY,
            JSON.stringify(response?.data),
          );
          await setFcmToken(fcmToken);
        } else {
          dispatch(toggleLoading(false));
          setModalShown(true);
          dispatch(
            toggleModal(
              true,
              ModalTexts.GENERAL_ERROR_TITLE,
              ModalTexts.GENERAL_ERROR_DESCRIPTION,
            ),
          );
        }
      })
      .catch(error => {
        dispatch(toggleLoading(false));
        console.log('error login', error);
        dispatch(
          toggleModal(true, ModalTexts.EMPTY_FIELD_TITLE, error?.data?.message),
        );
        setModalShown(true);
      });
  };

  return (
    <>
      <ModalCard
        modalShown={modalShown}
        onBackdropPress={() => setModalShown(false)}
        isBasicModal={true}
        cancelButton={ModalTexts.MODAL_OK}
      />
      {loading && <LoadingSpinner />}

      <View
        style={{
          flex: 1,
          alignItems: 'center',
          backgroundColor: Colors.COLOR_OFF_WHITE,
        }}>
        <View
          style={{
            width: ScreenDimensions.WIDTH,
            maxHeight: ScreenDimensions.HEIGHT / 3,
          }}>
          <Image
            source={require('../../assets/images/green-plants.jpg')}
            style={{ width: '100%', height: '100%' }}
            resizeMode="cover"
            borderBottomRightRadius={40}
            borderBottomLeftRadius={40}
          />
        </View>

        <KeyboardAwareScrollView
          bounces={false}
          showsVerticalScrollIndicator={false}
          enableOnAndroid={true}
          scrollEnabled={true}
          extraScrollHeight={0}
          scrollToOverflowEnabled={true}
          enableAutomaticScroll={true}
          style={{
            backgroundColor: Colors.COLOR_OFF_WHITE,
            width: '100%',
            paddingHorizontal: 20,
            height: ScreenDimensions.HEIGHT / 1.5,
            position: 'absolute',
            bottom: 0,
            paddingTop: 60,
          }}
          keyboardShouldPersistTaps="handled">
          <View>
            <View style={{ marginBottom: 30 }}>
              <Text
                style={{
                  textAlign: 'center',
                  fontWeight: '700',
                  fontSize: 36,
                  marginBottom: 10,
                  color: Colors.COLOR_PRIMARY_GREEN,
                }}>
                {t('common:login_screen_welcome')}
              </Text>
              <Text
                style={{
                  textAlign: 'center',
                  fontWeight: '400',
                  fontSize: 18,
                  color: Colors.TEXT_GRAY,
                }}>
                {t('common:login_description_login_screen')}
              </Text>
            </View>

            <View
              style={{
                width: '100%',
              }}>
              <Formik
                initialValues={{ email: '', password: '' }}
                onSubmit={email => {
                  onSubmitPress(email);
                }}
                validationSchema={LoginSchema}>
                {({
                  handleChange,
                  handleBlur,
                  handleSubmit,
                  values,
                  errors,
                  touched,
                }) => (
                  <View style={{ width: '100%', paddingBottom: 50 }}>
                    <View style={{ marginBottom: 15 }}>
                      <FormInput
                        handleChange={handleChange('email')}
                        handleBlur={handleBlur('email')}
                        value={values.email}
                        placeholder={t('common:email_placeholder')}
                      />
                      <InputValidationCircle
                        error={errors.email}
                        touched={touched.email}
                        value={values.email}
                      />
                      <View style={{ position: 'absolute', top: 9, left: 10 }}>
                        <Icon
                          name="email"
                          size={30}
                          color={Colors.COLOR_PRIMARY_GREEN}
                        />
                      </View>
                      {errors.email && touched.email && (
                        <Text
                          style={{
                            color: Colors.COLOR_DANGER,
                            marginTop: 5,
                          }}>
                          {errors.email}
                        </Text>
                      )}
                    </View>
                    <View style={{ marginBottom: 15 }}>
                      <FormInput
                        handleChange={handleChange('password')}
                        handleBlur={handleBlur('password')}
                        value={values.password}
                        secureEntry={hidePass ? true : false}
                        placeholder={t('common:password_placeholder')}
                        noCapitalize={true}
                      />

                      <InputValidationCircle
                        error={errors.password}
                        touched={touched.password}
                        value={values.password}
                      />
                      <View style={{ position: 'absolute', top: 10, left: 10 }}>
                        <Icon
                          name="lock"
                          size={28}
                          color={Colors.COLOR_PRIMARY_GREEN}
                        />
                      </View>
                      <View
                        style={{ position: 'absolute', top: 13, right: 40 }}>
                        <Icon
                          type="font-awesome-5"
                          name={hidePass ? 'eye-slash' : 'eye'}
                          size={23}
                          color={Colors.COLOR_PRIMARY_GREEN}
                          onPress={() => setHidePass(!hidePass)}
                        />
                      </View>
                      {errors.password && touched.password && (
                        <Text
                          style={{
                            color: Colors.COLOR_DANGER,
                            marginTop: 5,
                          }}>
                          {errors.password}
                        </Text>
                      )}
                    </View>

                    <CustomButton
                      title={t('common:login_button_login_screen')}
                      onPress={() => handleSubmit(values.email)}
                    />
                    <View
                      style={{
                        alignItems: 'center',
                        justifyContent: 'center',
                        flexDirection: 'row',
                      }}>
                      <Text>{t('common:register_redirect_login_screen')}</Text>
                      <TouchableOpacity
                        onPress={() => props.navigation.navigate('Sign Up')}>
                        <Text
                          style={{
                            color: Colors.COLOR_PRIMARY_GREEN,
                            fontWeight: 'bold',
                            textDecorationLine: 'underline',
                            marginStart: 2,
                          }}>
                          {t('common:register_header_register_screen')}
                        </Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                )}
              </Formik>
            </View>
          </View>
        </KeyboardAwareScrollView>
      </View>
    </>
  );
};

export default LoginScreen;
