import React, { useState } from 'react';
import { View, TouchableOpacity, Image } from 'react-native';
import { Text } from 'react-native-elements';
import { useDispatch, useSelector } from 'react-redux';
import * as authActions from '../../store/actions/auth';
import { register, setFcmToken } from '../../infrastructure/api/api';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {
  AsyncStorageKeys,
  Colors,
  ModalTexts,
  ScreenDimensions,
} from '../../shared/constants';
import { toggleLoading } from '../../store/actions/loadingActions';
import LoadingSpinner from '../../components/loading/LoadingSpinner';
import { toggleModal } from '../../store/actions/modal';
import ModalCard from '../../components/modal/Modal';
import { Icon } from 'react-native-elements';
import { Formik } from 'formik';
import * as Yup from 'yup';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import FormInput from '../../components/form-input/FormInput';
import InputValidationCircle from '../../components/input-validation-circle/InputValidationCircle';
import CustomButton from '../../components/custom-button/CustomButton';
import { useTranslation } from 'react-i18next';

const RegisterScreen = props => {
  const dispatch = useDispatch();
  const loading = useSelector(state => state.loading.loadingState);
  const [hidePass, setHidePass] = useState(true);
  const [modalShown, setModalShown] = useState(false);

  const { i18n, t } = useTranslation();

  const RegisterSchema = Yup.object().shape({
    name: Yup.string()
      .min(5, t('common:username_chars_error_register_screen'))
      .max(50, t('common:username_long_error_register_screen'))
      .required(t('common:username_required_error_register_screen')),
    email: Yup.string()
      .email(t('common:email_invalid_error_login_register_screen'))
      .required(t('common:email_required_error_login_register_screen')),
    phone: Yup.string()
      .matches(/^01[0-5]\d{8}$/, {
        message: t('common:phone_number_invalid_error_register_screen'),
        excludeEmptyString: false,
      })
      .required(t('common:phone_number_required_error_register_screen')),
    password: Yup.string()
      .min(8, t('common:password_short_error_login_register_screen'))
      .max(50, t('common:password_long_error_login_register_screen'))
      .required(t('common:password_required_error_login_register_screen')),
  });

  const setLanguage = code => {
    return i18n.changeLanguage(code);
  };

  const onSubmitPress = values => {
    dispatch(toggleLoading(true));

    register(
      values.name,
      values.email.toLowerCase(),
      values.password,
      values.phone,
    )
      .then(async response => {
        if (response?.data) {
          await AsyncStorage.setItem(
            AsyncStorageKeys.USER_KEY,
            JSON.stringify(response?.data),
          );
          dispatch(toggleLoading(false));

          await dispatch(authActions.setCurrentUser(response?.data));
          let fcmToken = await AsyncStorage.getItem('fcmToken');
          await setFcmToken(fcmToken);
        } else {
          dispatch(toggleLoading(false));
          setModalShown(true);
          dispatch(
            toggleModal(
              true,
              ModalTexts.GENERAL_ERROR_TITLE,
              ModalTexts.GENERAL_ERROR_DESCRIPTION,
              true,
            ),
          );
        }
      })
      .catch(error => {
        dispatch(toggleLoading(false));
        const firstError = Object.keys(error?.data?.errors)[0];
        setModalShown(true);
        dispatch(
          toggleModal(
            true,
            ModalTexts.EMPTY_FIELD_TITLE,
            error.data.errors[firstError].toString() ?? error.data.message,
            true,
          ),
        );
      });
  };

  return (
    <>
      <ModalCard
        modalShown={modalShown}
        onBackdropPress={() => setModalShown(false)}
        isBasicModal={true}
        cancelButton={ModalTexts.MODAL_OK}
      />
      {loading && <LoadingSpinner />}

      <View
        style={{
          flex: 1,
          alignItems: 'center',
          backgroundColor: Colors.COLOR_OFF_WHITE,
        }}
      >
        <View
          style={{
            width: 120,
            height: 90,
            position: 'absolute',
            left: -22,
            top: 50,
            zIndex: 100,
          }}
        >
          <Image
            source={require('../../assets/images/green-leaf-cropped.png')}
            style={{ width: '100%', height: '100%' }}
            resizeMode="contain"
          />
        </View>
        <KeyboardAwareScrollView
          bounces={false}
          showsVerticalScrollIndicator={false}
          enableOnAndroid={true}
          scrollEnabled={true}
          extraScrollHeight={0}
          scrollToOverflowEnabled={true}
          enableAutomaticScroll={true}
          style={{
            backgroundColor: Colors.COLOR_OFF_WHITE,
            width: '100%',
            paddingHorizontal: 20,
            height: ScreenDimensions.HEIGHT,
            position: 'absolute',
            bottom: 0,
            paddingTop: 160,
          }}
          keyboardShouldPersistTaps="handled"
        >
          <View style={{ marginBottom: 30 }}>
            <Text
              style={{
                textAlign: 'center',
                fontWeight: '700',
                fontSize: 36,
                marginBottom: 10,
                color: Colors.COLOR_PRIMARY_GREEN,
              }}
            >
              {t('common:register_header_register_screen')}
            </Text>
            <Text
              style={{
                textAlign: 'center',
                fontWeight: '400',
                fontSize: 18,
                color: Colors.TEXT_GRAY,
              }}
            >
              {t('common:register_description_register_screen')}
            </Text>
          </View>

          <View
            style={{
              width: '100%',
            }}
          >
            <Formik
              initialValues={{ name: '', email: '', phone: '', password: '' }}
              onSubmit={email => {
                onSubmitPress(email);
              }}
              validationSchema={RegisterSchema}
            >
              {({
                handleChange,
                handleBlur,
                handleSubmit,
                values,
                errors,
                touched,
              }) => (
                <View
                  style={{
                    width: '100%',
                    paddingBottom: 50,
                    height: ScreenDimensions.HEIGHT / 1.5,
                  }}
                >
                  <View style={{ marginBottom: 15 }}>
                    <FormInput
                      handleChange={handleChange('name')}
                      handleBlur={handleBlur('name')}
                      value={values.name}
                      placeholder={t('common:name_placeholder')}
                    />
                    <InputValidationCircle
                      error={errors.name}
                      touched={touched.name}
                      value={values.name}
                    />
                    <View style={{ position: 'absolute', top: 9, left: 10 }}>
                      <Icon
                        name="person"
                        size={30}
                        color={Colors.COLOR_PRIMARY_GREEN}
                      />
                    </View>
                    {errors.name && touched.name && (
                      <Text
                        style={{
                          color: Colors.COLOR_DANGER,
                          marginTop: 5,
                        }}
                      >
                        {errors.name}
                      </Text>
                    )}
                  </View>
                  <View style={{ marginBottom: 15 }}>
                    <FormInput
                      handleChange={handleChange('email')}
                      handleBlur={handleBlur('email')}
                      value={values.email}
                      placeholder={t('common:email_placeholder')}
                    />
                    <InputValidationCircle
                      error={errors.email}
                      touched={touched.email}
                      value={values.email}
                    />
                    <View style={{ position: 'absolute', top: 9, left: 10 }}>
                      <Icon
                        name="email"
                        size={30}
                        color={Colors.COLOR_PRIMARY_GREEN}
                      />
                    </View>
                    {errors.email && touched.email && (
                      <Text
                        style={{
                          color: Colors.COLOR_DANGER,
                          marginTop: 5,
                        }}
                      >
                        {errors.email}
                      </Text>
                    )}
                  </View>
                  <View style={{ marginBottom: 15 }}>
                    <FormInput
                      handleChange={handleChange('phone')}
                      handleBlur={handleBlur('phone')}
                      value={values.phone}
                      placeholder={t('common:phone_number_placeholder')}
                      isNumber={true}
                    />
                    <InputValidationCircle
                      error={errors.phone}
                      touched={touched.phone}
                      value={values.phone}
                    />
                    <View style={{ position: 'absolute', top: 9, left: 10 }}>
                      <Icon
                        name="phone"
                        size={30}
                        color={Colors.COLOR_PRIMARY_GREEN}
                      />
                    </View>
                    {errors.phone && touched.phone && (
                      <Text
                        style={{
                          color: Colors.COLOR_DANGER,
                          marginTop: 5,
                        }}
                      >
                        {errors.phone}
                      </Text>
                    )}
                  </View>
                  <View style={{ marginBottom: 15 }}>
                    <FormInput
                      handleChange={handleChange('password')}
                      handleBlur={handleBlur('password')}
                      value={values.password}
                      secureEntry={hidePass ? true : false}
                      placeholder={t('common:password_placeholder')}
                      noCapitalize={true}
                    />

                    <InputValidationCircle
                      error={errors.password}
                      touched={touched.password}
                      value={values.password}
                    />
                    <View style={{ position: 'absolute', top: 10, left: 10 }}>
                      <Icon
                        name="lock"
                        size={28}
                        color={Colors.COLOR_PRIMARY_GREEN}
                      />
                    </View>
                    <View style={{ position: 'absolute', top: 13, right: 40 }}>
                      <Icon
                        type="font-awesome-5"
                        name={hidePass ? 'eye-slash' : 'eye'}
                        size={23}
                        color={Colors.COLOR_PRIMARY_GREEN}
                        onPress={() => setHidePass(!hidePass)}
                      />
                    </View>
                    {errors.password && touched.password && (
                      <Text
                        style={{
                          color: Colors.COLOR_DANGER,
                          marginTop: 5,
                        }}
                      >
                        {errors.password}
                      </Text>
                    )}
                  </View>
                  <CustomButton
                    title={t('common:register_button_register_screen')}
                    onPress={() => handleSubmit(values.email)}
                  />
                  <View
                    style={{
                      alignItems: 'center',
                      justifyContent: 'center',
                      flexDirection: 'row',
                    }}
                  >
                    <Text>{t('common:login_redirect_register_screen')}</Text>
                    <TouchableOpacity
                      onPress={() => props.navigation.navigate('Login')}
                    >
                      <Text
                        style={{
                          color: Colors.COLOR_PRIMARY_GREEN,
                          fontWeight: 'bold',
                          textDecorationLine: 'underline',
                          marginStart: 2,
                        }}
                      >
                        {t('common:login_button_login_screen')}
                      </Text>
                    </TouchableOpacity>
                  </View>
                </View>
              )}
            </Formik>
          </View>
        </KeyboardAwareScrollView>
      </View>
    </>
  );
};

export default RegisterScreen;
