import React, { useEffect, useState } from 'react';
import { View, StyleSheet, Image, ScrollView } from 'react-native';
import { Colors, ScreenDimensions } from '../../shared/constants';
import LinearGradient from 'react-native-linear-gradient';
import { getLeaderboardUsers } from '../../infrastructure/api/api';
import UserPointCard from '../../components/UserPointCard';
import _ from 'lodash';

export default function LeaderboardScreen() {
  const [loading, setLoading] = useState(false);
  const [users, setUsers] = useState([]);

  useEffect(() => {
    loadUsers();
  }, []);

  const loadUsers = async () => {
    try {
      setLoading(true);
      let response = await getLeaderboardUsers();
      // console.log(response.data);
      setUsers(response.data.splice(0, 10));
      setLoading(false);
    } catch (e) {
      console.log(e);
      setLoading(false);
      return;
    }
  };

  const addRankAttrToUsers = data => {
    let topThree = getDistinctTopThreePoints();

    users
      .map((element, index) => {
        if (element.points === topThree[0]) {
          element['rank'] = 1;
        }
        if (element.points === topThree[1]) {
          element['rank'] = 2;
        }
        if (element.points === topThree[2]) {
          element['rank'] = 3;
        }
        if (!topThree.includes(element.points)) {
          element['rank'] = 4;
        }
      })
      .filter(element => element >= 0);
  };

  const getDistinctTopThreePoints = () => {
    let Uniq = _.uniq(_.map(users, 'points'));
    let topThree = Uniq.slice(0, 3);

    return topThree;
  };

  addRankAttrToUsers(users);

  return (
    <ScrollView contentContainerStyle={{ flex: 1 }}>
      <LinearGradient
        colors={[Colors.COLOR_PRIMARY, Colors.lightBg]}
        style={{ flex: 1, elevation: 0 }}
      >
        <View style={styles.container}>
          <View
            style={{
              height: '35%',
              alignItems: 'center',
              justifyContent: 'center',
              paddingHorizontal: 15,
            }}
          >
            <Image
              source={require('../../assets/confetti.png')}
              style={{ width: '100%', height: '100%' }}
              resizeMode="stretch"
              borderBottomRightRadius={40}
              borderBottomLeftRadius={40}
            />
            <View
              style={{
                position: 'absolute',
                flexDirection: 'row',
                alignItems: 'center',
              }}
            >
              <UserPointCard
                key={users[0]?.id}
                name={users[0]?.name}
                points={users[0]?.points}
                rank={users[0]?.rank}
              />
            </View>
          </View>

          <View style={styles.bodyContainer}>
            <ScrollView
              contentContainerStyle={{ paddingBottom: 90 }}
              showsVerticalScrollIndicator={false}
            >
              {users.map((user, index) => {
                return index > 0 ? (
                  <UserPointCard
                    key={user.id}
                    name={user.name}
                    points={user.points}
                    rank={user.rank}
                  />
                ) : null;
              })}
            </ScrollView>
          </View>
        </View>
      </LinearGradient>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

  bodyContainer: {
    backgroundColor: Colors.lightBg,
    flex: 1,
    paddingHorizontal: 30,
    borderTopRightRadius: 45,
    borderTopLeftRadius: 45,
    shadowColor: 'black',
    elevation: 45,
    paddingTop: 30,
  },
});
