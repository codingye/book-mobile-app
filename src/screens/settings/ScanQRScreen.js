import React, { useState } from 'react';
import { ModalTexts } from '../../shared/constants';
import LoadingSpinner from '../../components/loading/LoadingSpinner';
import QRCodeScanner from 'react-native-qrcode-scanner';
import { RNCamera } from 'react-native-camera';
import { confirmTransaction, getUser } from '../../infrastructure/api/api';
import ModalCard from '../../components/modal/Modal';
import { useDispatch } from 'react-redux';
import { toggleModal } from '../../store/actions/modal';
import * as authActions from '../../store/actions/auth';

const ScanQRScreen = props => {
  const [loading, setLoading] = useState(false);
  const [modalShown, setModalShown] = useState(false);
  const dispatch = useDispatch();

  const onSuccess = async e => {
    setLoading(true);
    try {
      const { transactionId, code } = JSON.parse(e.data);
      const res = await confirmTransaction(transactionId, code);
      if (res.data.confirmed) {
        setLoading(false);
        dispatch(
          toggleModal(
            true,
            ModalTexts.QR_CODE_CONFIRM_TITLE,
            ModalTexts.QR_CODE_CONFIRM_DESCRIPTION,
          ),
        );
        const userResponse = await getUser();
        const { points } = userResponse.data;
        dispatch(authActions.updateUserPoints(points));
        setModalShown(true);
      }
    } catch (e) {
      setLoading(false);
      dispatch(
        toggleModal(
          true,
          ModalTexts.GENERAL_ERROR_TITLE,
          ModalTexts.GENERAL_ERROR_DESCRIPTION,
        ),
      );
      setModalShown(true);
      props.navigation.pop();
    }
  };

  return (
    <>
      {loading && <LoadingSpinner />}
      <ModalCard
        modalShown={modalShown}
        onBackdropPress={() => setModalShown(false)}
        isBasicModal={true}
        cancelButton={ModalTexts.MODAL_OK}
      />
      <QRCodeScanner
        onRead={onSuccess}
        flashMode={RNCamera.Constants.FlashMode.off}
      />
    </>
  );
};

export default ScanQRScreen;
