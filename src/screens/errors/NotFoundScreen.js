import React from 'react';
import { Text, StyleSheet, View } from 'react-native';

const SettingsHomeScreen = props => {
  return (
    <View style={styles.container}>
      <Text style={styles.centered}>Not Found</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  centered: {
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default SettingsHomeScreen;
