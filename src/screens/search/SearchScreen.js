import { Text, TextInput, TouchableOpacity, View } from 'react-native';
import React, { useState } from 'react';
import { Colors } from '../../shared/constants';
import { Icon as Icons } from 'react-native-elements';
import { searchPosts } from '../../infrastructure/api/api';
import { FlatList } from 'react-native-gesture-handler';
import PostResult from '../../components/PostResult';

const SearchScreen = props => {
  const [term, setTerm] = useState('');
  const [results, setResults] = useState([]);

  const searchFor = async () => {
    if (!term) return;
    console.log('searchterm', term);
    const response = await searchPosts(term);
    setResults(response.data.data);
    console.log('response result', response.data.data);
  };

  const onSelectPost = post => {
    props.navigation.navigate('MyPostDetailsScreen', { post: post });
  };

  const onChangeText = newText => {
    setTerm(newText);
    if (newText.length % 3 == 0 && newText.trim()) searchFor();
  };

  const renderPostResult = ({ item }) => {
    return (
      <PostResult
        title={item.title}
        price={item.price}
        description={item.description}
        imageURL={item.image_url}
        createdAt={item.created_at_readable}
        postOwner={item.user.name}
        postType={item.post_type_readable}
        onSelect={() => onSelectPost(item)}
      />
    );
  };

  return (
    <View
      style={{
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        paddingTop: 70,
        paddingHorizontal: 20,
      }}
    >
      <Text
        numberOfLines={0}
        style={{
          fontSize: 24,
          fontWeight: 'bold',
          color: Colors.COLOR_PRIMARY_GREEN,
          textAlign: 'left',
        }}
      >
        What Book Are You Looking For?
      </Text>

      <View
        style={{
          width: '100%',
          flexDirection: 'row',
          justifyContent: 'space-between',
          marginTop: 35,
          elevation: 25,
          borderRadius: 10,
        }}
      >
        <TextInput
          style={{
            width: '83%',
            borderRadius: 10,
            fontSize: 18,
            fontWeight: '600',
            padding: 4,
            paddingLeft: 8,
            backgroundColor: Colors.COLOR_OFF_WHITE,
            color: Colors.COLOR_PRIMARY_GREEN,
          }}
          onChangeText={onChangeText}
          onEndEditing={searchFor}
          placeholder="Search for book"
        />
        <View
          style={{
            backgroundColor: Colors.COLOR_GREEN_INACTIVE,
            padding: 8,
            alignItems: 'center',
            justifyContent: 'center',
            borderRadius: 10,
            width: '15%',
            opacity: term?.trim().length >= 1 ? 1 : 0.8,
          }}
        >
          <TouchableOpacity onPress={searchFor} disabled={!term}>
            <Icons
              type="font-awesome-5"
              name="search"
              size={30}
              color="white"
            />
          </TouchableOpacity>
        </View>
      </View>

      <FlatList
        initialNumToRender={2}
        windowSize={4}
        contentContainerStyle={{ paddingBottom: 160, marginTop: 40 }}
        style={{ flexGrow: 1 }}
        showsVerticalScrollIndicator={false}
        data={results}
        renderItem={renderPostResult}
        // onEndReached={!bottomLoader ? loadMorePosts : null}
        // onEndReachedThreshold={0}
      />
    </View>
  );
};

//No Results To Show
//Please check spelling or try different keywords.

export default SearchScreen;
