import { View, StyleSheet, FlatList, RefreshControl, Text } from 'react-native';
import React, { useEffect, useState } from 'react';
import { getNotifications, getSinglePost } from '../../infrastructure/api/api';
import NotificationCard from '../../components/notification/NotificationCard';
import { Colors, ModalTexts } from '../../shared/constants';
import LoadingSpinner from '../../components/loading/LoadingSpinner';
import Icon from 'react-native-vector-icons/Entypo';
import CustomButtonSecondary from '../../components/custom-button/CustomButton';
import ModalCard from '../../components/modal/Modal';
import { useDispatch, useSelector } from 'react-redux';
import { toggleModal } from '../../store/actions/modal';
import { useTranslation } from 'react-i18next';

const NotificationsHomeScreen = ({ navigation }) => {
  const [notificationsArray, setNotificationsArray] = useState([]);
  const [refreshing, setRefreshing] = useState(false);
  const [loading, setLoading] = useState(false);
  const [modalShown, setModalShown] = useState(false);
  const notificationFlag = useSelector(
    state => state.notification.notificationFlag,
  );
  const dispatch = useDispatch();

  const { i18n, t } = useTranslation();
  const setLanguage = code => {
    return i18n.changeLanguage(code);
  };

  const loadNotifications = async (loadSpinner = true) => {
    try {
      loadSpinner ? setLoading(true) : null;
      let response = await getNotifications();
      setNotificationsArray([]);
      setNotificationsArray(response.data);
      loadSpinner ? setLoading(false) : null;
    } catch (e) {
      loadSpinner ? setLoading(false) : null;
      dispatch(
        toggleModal(
          true,
          ModalTexts.EMPTY_FIELD_TITLE,
          ModalTexts.GENERAL_ERROR_DESCRIPTION,
        ),
      );
      setModalShown(true);
      return;
    }
  };

  useEffect(() => {
    loadNotifications();
  }, [notificationFlag]);

  const onRefresh = async () => {
    setRefreshing(true);
    await loadNotifications(false);
    setRefreshing(false);
  };

  const onNotificationClicked = async postId => {
    try {
      setLoading(true);
      let notificationPost = await getSinglePost(postId);
      setLoading(false);
      console.log('this is', notificationPost);
      navigation.navigate('MyPostDetailsScreen', {
        post: notificationPost.data,
      });
    } catch (error) {
      setLoading(false);
      dispatch(
        toggleModal(
          true,
          ModalTexts.EMPTY_FIELD_TITLE,
          ModalTexts.GENERAL_ERROR_DESCRIPTION,
        ),
      );
      setModalShown(true);
    }
  };

  const notificationCardRenderer = item => {
    let isDisabled = item.details.length == 0 ? true : false;
    return (
      <NotificationCard
        title={item.title}
        body={item.body}
        date={item.created_at_readable}
        isDisabled={isDisabled}
        onClick={
          isDisabled ? null : () => onNotificationClicked(item.details.post_id)
        }
      />
    );
  };

  if (notificationsArray.length < 1 && !loading) {
    return (
      <View style={styles.emptyContainer}>
        <View style={styles.iconWithTextsContainer}>
          <Icon
            name="bell"
            size={100}
            color={Colors.COLOR_PRIMARY}
            style={{ textShadowOffset: { width: 3, height: 2 } }}
          />
          <View style={styles.textContainer}>
            <Text style={styles.titleText}>
              {t(`common:no_notification_title_notification_screen`)}
            </Text>
            <Text style={styles.descriptionText}>
              {t(`common:no_notification_description_notification_screen`)}
            </Text>
          </View>
        </View>
        <CustomButtonSecondary
          title={t(`common:refresh_button`)}
          onPress={loadNotifications}
        />
      </View>
    );
  }

  return (
    <>
      <ModalCard
        modalShown={modalShown}
        onBackdropPress={() => setModalShown(false)}
        isBasicModal={true}
        cancelButton={ModalTexts.MODAL_OK}
      />
      <View style={styles.containerStyle}>
        <FlatList
          style={{ width: '100%' }}
          data={notificationsArray}
          contentContainerStyle={{ paddingBottom: 70 }}
          keyExtractor={item => item.id}
          renderItem={({ item }) => notificationCardRenderer(item)}
          refreshControl={
            <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
          }
        />
      </View>
      {loading && <LoadingSpinner />}
    </>
  );
};
const styles = StyleSheet.create({
  containerStyle: {
    justifyContent: 'center',
    flex: 1,
    alignItems: 'center',
    backgroundColor: Colors.lightBg,
    paddingBottom: 80,
  },

  emptyContainer: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: Colors.lightBg,
  },

  iconWithTextsContainer: { alignItems: 'center' },

  textContainer: { marginTop: 20, paddingBottom: 10, alignItems: 'center' },

  titleText: {
    fontSize: 22,
    color: Colors.textThird,
    fontWeight: '700',
  },

  descriptionText: { color: Colors.textThird },
});

export default NotificationsHomeScreen;
