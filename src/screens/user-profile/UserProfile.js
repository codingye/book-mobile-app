import { StyleSheet, Text, View } from 'react-native';
import React from 'react';
import { Colors, ScreenDimensions } from '../../shared/constants';
import helpers from '../../shared/helpers';
import { useSelector } from 'react-redux';
import { Icon } from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import { useTranslation } from 'react-i18next';

const UserProfile = () => {
  const user = useSelector(state => state.user.currentUser);
  const postsCount = useSelector(state => state.user.numberOfPosts);

  const { i18n, t } = useTranslation();
  const setLanguage = code => {
    return i18n.changeLanguage(code);
  };

  return (
    <LinearGradient
      colors={[Colors.COLOR_PRIMARY, Colors.lightBg]}
      style={{ flex: 1, elevation: 0 }}
    >
      <View style={styles.container}>
        <View style={styles.header} />

        <View style={styles.bodyContainer}>
          <View style={styles.iconAndNameContainer}>
            <View style={styles.avatarContainer}>
              <View style={styles.editIconContainer}>
                <Icon
                  type="font-awesome-5"
                  name="pen"
                  color={Colors.COLOR_PRIMARY}
                  size={16}
                />
              </View>
              <Icon
                type="material-community"
                name="account"
                color={Colors.textLightGray}
                size={50}
              />
            </View>

            <View
              style={{
                alignItems: 'center',
                justifyContent: 'center',
                marginTop: 10,
              }}
            >
              <Text style={styles.userName}>{user.name}</Text>
              <Text
                style={{
                  color: Colors.textThird,
                  fontFamily: 'Poppins-Regular',
                }}
              >
                {user.email.toUpperCase()}
              </Text>
            </View>

            <View style={styles.detailsContainer}>
              <View style={styles.itemDetailsContainer}>
                <Text style={styles.itemDetailsTitle}>{postsCount}</Text>
                <Text style={styles.itemDetailsDescription}>
                  {t(`common:count_posts_profile_screen`)}
                </Text>
              </View>
              <View style={styles.divider} />
              <View style={styles.itemDetailsContainer}>
                <Text style={styles.itemDetailsTitle}>
                  {helpers.parseUserProfileDate(user.created_at)}
                </Text>
                <Text style={styles.itemDetailsDescription}>
                  {t(`common:joined_since_profile_screen`)}
                </Text>
              </View>
            </View>
          </View>
        </View>
      </View>
    </LinearGradient>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    height: '25%',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 15,
  },
  avatarContainer: {
    backgroundColor: Colors.COLOR_PRIMARY,
    width: ScreenDimensions.WIDTH / 4.2,
    height: ScreenDimensions.WIDTH / 4.2,
    borderRadius: ScreenDimensions.WIDTH / 12,
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: Colors.COLOR_PRIMARY,
    elevation: 15,
    borderColor: Colors.textLightGray,
    borderWidth: 3,
  },

  editIconContainer: {
    position: 'absolute',
    width: 25,
    height: 25,
    borderRadius: 12.5,
    backgroundColor: Colors.textLightGray,
    bottom: -5,
    right: -5,
    borderWidth: 2,
    borderColor: Colors.COLOR_PRIMARY,
    alignItems: 'center',
    justifyContent: 'center',
  },

  userName: {
    color: 'rgba(1,1,1,0.75)',
    fontSize: 28,
    fontFamily: 'Poppins-Regular',
  },

  bodyContainer: {
    backgroundColor: Colors.lightBg,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 30,
    borderTopRightRadius: 45,
    borderTopLeftRadius: 45,
    shadowColor: 'black',
    elevation: 45,
    paddingTop: 70,
  },

  iconAndNameContainer: {
    position: 'absolute',
    top: -ScreenDimensions.WIDTH / 4.2 / 2,
    justifyContent: 'center',
    alignItems: 'center',
  },

  detailsContainer: {
    flexDirection: 'row',
    marginTop: 30,
    width: ScreenDimensions.WIDTH / 1.9,
    height: ScreenDimensions.HEIGHT / 19,
    justifyContent: 'space-between',
    alignItems: 'center',
  },

  itemDetailsContainer: {
    width: '49%',
    justifyContent: 'space-between',
    height: '100%',
    alignItems: 'center',
  },

  itemDetailsTitle: {
    color: Colors.COLOR_GREEN_INACTIVE,
    fontWeight: '800',
    fontSize: 20,
  },

  itemDetailsDescription: {
    color: Colors.textThird,
    fontWeight: '400',
    fontSize: 14,
  },
  divider: {
    height: '100%',
    width: '0.5%',
    backgroundColor: Colors.COLOR_GREEN_INACTIVE,
    borderRadius: 25,
  },
});

export default UserProfile;
