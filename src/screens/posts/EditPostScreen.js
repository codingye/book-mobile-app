import {
  StyleSheet,
  View,
  ScrollView,
  Keyboard,
  KeyboardAvoidingView,
} from 'react-native';
import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import InputEdit from '../../components/edit-post/InputEdit';
import Label from '../../components/post/Label';
import CustomDropdown from '../../components/post/CustomDropdown';
import {
  editPost,
  uploadImage,
  getPostTypes,
} from '../../infrastructure/api/api';
import { ImagePicker } from '../../components/ImagePicker';
import { Button } from 'react-native-elements';
import { Colors, ScreenDimensions } from '../../shared/constants';
import LoadingSpinner from '../../components/loading/LoadingSpinner';
import { toggleReaload } from '../../store/actions/loadingActions';
import CustomButtonSecondary from '../../components/custom-button/CustomButton';
import { useTranslation } from 'react-i18next';

const EditPost = props => {
  const { post } = props.route.params;
  const reload = useSelector(state => state.loading.reload);
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(false);
  const [postTypes, setPostTypes] = useState([]);

  let objectOfValuesOriginalPost = {
    title: post.title,
    description: post.description,
    postType: post.post_type_id,
    price: post.price,
    exchange_details: post.exchange_details,
    pickup_location: post.pickup_location,
    contact_method: post.contact_method,
    image_url: post.image_url,
  };
  const [values, setValues] = useState({
    title: post.title,
    description: post.description,
    postType: post.post_type_id,
    price: post.price,
    exchange_details: post.exchange_details,
    pickup_location: post.pickup_location,
    contact_method: post.contact_method,
  });
  const [image, setImage] = useState(post.image_url);

  const [errorsObj, setErrorsObj] = useState({
    title: false,
    description: false,
    price: false,
    exchange_details: false,
    pickup_location: false,
    image: false,
  });

  const { i18n, t } = useTranslation();

  const setLanguage = code => {
    return i18n.changeLanguage(code);
  };

  const [selectedContactMethod, setSelectedContactMethod] = useState(
    t(`common:contact_method_whatsApp_label`),
  );

  const dataContactMethods = [
    { label: t(`common:contact_method_whatsApp_label`), value: 'WHATSAPP' },
    { label: t(`common:contact_method_phone_label`), value: 'PHONE' },
    { label: t(`common:contact_method_both_label`), value: 'BOTH' },
  ];

  const minimumLengthError = t(`common:minimum_length_field_add_post`);

  const DATA = [
    {
      name: 'title',
      label: t(`common:title_label`),
      placeholder: t(`common:title_placeholder`),
      iconName: 'format-title',
      iconType: 'material-community',
      errorMessage: `${t(`common:field_is_required`)}\n${minimumLengthError}`,
      autoFocus: true,
    },
    {
      name: 'description',
      label: t(`common:description_label`),
      placeholder: t(`common:description_placeholder`),
      iconName: 'comment-dots',
      iconType: 'font-awesome-5',
      errorMessage: `${t(`common:field_is_required`)}\n${minimumLengthError}`,
    },
    {
      name: 'price',
      label: t(`common:price_label`),
      placeholder: t(`common:price_placeholder`),
      iconName: 'comment-dots',
      iconType: 'font-awesome-5',
      keyboardType: 'numeric',
      condition:
        postTypes.find(type => type.value === values.postType)?.label ===
        t(`common:post_type_sell_label`),
      errorMessage: t(`common:field_is_required`),
    },
    {
      name: 'exchange_details',
      label: t(`common:exchange_details_label`),
      placeholder: t(`common:exchange_details_placeholder`),
      iconName: 'comment-dots',
      iconType: 'font-awesome-5',
      condition:
        postTypes.find(type => type.value === values.postType)?.label ===
        t(`common:post_type_exchange_label`),
      errorMessage: `${t(`common:field_is_required`)}\n${minimumLengthError}`,
    },
    {
      name: 'image',
      conditionImage: true,
      errorMessage: t(`common:image_support`),
    },
    {
      name: 'contact_method',
      label: t(`common:contact_method_label`),
      iconName: 'card-account-phone-outline',
      iconType: 'material-community',
      dropdown: true,
      data: dataContactMethods,
    },
    {
      name: 'pickup_location',
      label: t(`common:location_label`),
      placeholder: t(`common:location_placeholder`),
      iconName: 'map-marker-alt',
      iconType: 'font-awesome-5',
      errorMessage: `${t(`common:field_is_required`)}\n${minimumLengthError}`,
    },
  ];

  useEffect(() => {
    const fetchDataPostTypes = async () => {
      let res = await getPostTypes();
      let dataPostType = [];
      let dataPostTypeLabels = [
        t(`common:post_type_sell_label`),
        t(`common:post_type_exchange_label`),
        t(`common:post_type_donate_label`),
      ];

      dataPostType = res.data.map(type => ({
        label: type.name,
        value: type.id,
      }));

      for (let i = 0; i < dataPostType.length; i++) {
        dataPostType[i].label = dataPostTypeLabels[i];
      }
      setPostTypes(dataPostType);
    };

    fetchDataPostTypes();
  }, []);

  const onChange = (e, inputName) => {
    setValues({ ...values, [inputName]: e });
  };

  const upload = async image => {
    try {
      const formData = prepareFormData(image);
      return await uploadImage(formData);
    } catch (e) {
      console.error(e);
    }
  };

  const prepareFormData = image => {
    const data = new FormData();
    data.append('image', {
      ...image,
      name: image.fileName,
      uri: Platform.OS === 'ios' ? image.uri.replace('file://', '') : image.uri,
    });
    return data;
  };

  const checkImageExtension = () => {
    let typeImage =
      image.constructor === Object ? image['uri'] : image.slice(0, -1);
    let extension = typeImage.substr(typeImage.lastIndexOf('.') + 1);
    let extensionAllowed = ['jpeg', 'png', 'jpg', 'gif', 'svg'];
    if (!extensionAllowed.includes(extension)) {
      errorsObj.image = true;
      return;
    }
    errorsObj.image = false;
  };

  const isErrorExist = () => {
    // checkImageExtension();
    return Object.values(errorsObj).includes(true);
  };

  const checkValuesUpdated = async () => {
    let objectOfUpdatedValues = {};
    let count = 0;
    let keysOfOriginalPost = Object.keys(objectOfValuesOriginalPost);
    for (const [key, value] of Object.entries(values)) {
      if (value !== objectOfValuesOriginalPost[keysOfOriginalPost[count]]) {
        objectOfUpdatedValues[key] = value;
      }
      count++;
    }
    if (image.constructor === Object) {
      let typeImage = image['uri'];
      let [nameOriginalImage, nameSetImage] = [
        objectOfValuesOriginalPost.image_url.slice(0, -1).split('/').pop(),
        typeImage.split('/').pop(),
      ];
      if (nameOriginalImage !== nameSetImage) {
        let uploadedImageRes = await upload(image);
        let { image_url } = uploadedImageRes.data;
        objectOfUpdatedValues.image_url = image_url;
      }
    }

    return objectOfUpdatedValues;
  };

  const submitHandler = async () => {
    setLoading(true);
    let isError = isErrorExist();
    if (isError) {
      setLoading(false);
      return;
    }
    let valuesUpdated = await checkValuesUpdated();
    if (
      Object.keys(valuesUpdated).length !== 0 &&
      valuesUpdated.constructor === Object
    ) {
      let res = await editPost(post.id, valuesUpdated);
      if (res.status == 200) {
        setLoading(false);
        dispatch(toggleReaload(!reload));
        props.navigation.pop(2);

        return;
      }
    }
    setLoading(false);
    dispatch(toggleReaload(!reload));
    props.navigation.pop(2);
    return;
  };

  return (
    <>
      {loading && <LoadingSpinner />}
      <KeyboardAvoidingView behavior={'height'} style={{ flex: 1 }}>
        <ScrollView
          style={styles.container}
          contentContainerStyle={{ flexGrow: 1 }}
          keyboardShouldPersistTaps={'handled'}
        >
          {DATA.map((input, index) => {
            if (input.condition === false) return;
            if (input.conditionImage === true)
              return (
                <ImagePicker
                  setImage={setImage.bind(this)}
                  error={!image ? true : false}
                  imageData={image}
                  errorMessage={input.errorMessage}
                />
              );

            return (
              <>
                <Label
                  key={input.name}
                  iconName={input.iconName}
                  iconType={input.iconType}
                  label={input.label}
                  requiredStar
                />
                {input.dropdown ? (
                  <CustomDropdown
                    key={index}
                    data={input.data}
                    selectedValue={values[input.name]}
                    onValueChange={itemValue =>
                      setValues({ ...values, [input.name]: itemValue })
                    }
                  />
                ) : (
                  <InputEdit
                    key={index}
                    {...input}
                    placeholderTextColor={'gray'}
                    onChangeText={e => onChange(e, input.name)}
                    value={values[input.name]}
                    setErrors={setErrorsObj.bind(this)}
                    errorsObject={errorsObj}
                    returnKeyType="done"
                    onSubmitEditing={Keyboard.dismiss}
                    inputStyle={
                      [
                        'description',
                        'pickup_location',
                        'exchange_details',
                      ].includes(input.name) && styles.inputMultiStyle
                    }
                  />
                )}
              </>
            );
          })}

          <View style={{ marginVertical: 25, marginBottom: 70 }}>
            <CustomButtonSecondary title="Save" onPress={submitHandler} />
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    </>
  );
};

export default EditPost;

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#ffffff',
    flex: 1,
  },
  item: {
    backgroundColor: '#f9c2ff',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  inputMultiStyle: {
    height: 98,
    textAlignVertical: 'top',
  },
  saveButton: {
    backgroundColor: Colors.COLOR_PRIMARY,
    borderRadius: 5,
  },
  saveButtonContainer: {
    marginHorizontal: 50,
    height: 50,
    width: 200,
    marginVertical: 10,
  },
});
