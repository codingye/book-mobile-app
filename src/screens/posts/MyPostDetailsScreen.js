import React, { useState } from 'react';
import {
  View,
  ScrollView,
  StyleSheet,
  Image,
  TouchableOpacity,
  Linking,
  Platform,
  Alert,
} from 'react-native';
import { Divider, SpeedDial } from 'react-native-elements';
import { useDispatch, useSelector } from 'react-redux';
import LoadingSpinner from '../../components/loading/LoadingSpinner';
import BuyButton from '../../components/post/BuyButton';
import { Colors, ModalTexts } from '../../shared/constants';
import helpers from '../../shared/helpers';
import { defaultImage } from '../../shared/constants';
import { Text } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome5';
import ModalCard from '../../components/modal/Modal';
import { toggleModal } from '../../store/actions/modal';
import { deletePost } from '../../infrastructure/api/api';
import { toggleReaload } from '../../store/actions/loadingActions';
import IconMaterial from 'react-native-vector-icons/MaterialCommunityIcons';
import { useTranslation } from 'react-i18next';

const MyPostDetailsScreen = props => {
  const post = props.route.params.post;
  const dispatch = useDispatch();
  const user = useSelector(state => state.user.currentUser);
  const [loading, setLoading] = useState(false);
  const [deleteVisible, setDeleteVisible] = useState(false);
  const [open, setOpen] = useState(false);
  const reload = useSelector(state => state.loading.reload);
  const [modalShown, setModalShown] = useState(false);

  const [typeContactMethod, setTypeContactMethod] = useState(
    post.contact_method,
  );

  const isUserOwnPost = user.id === post?.user_id;

  const { i18n, t } = useTranslation();
  const setLanguage = code => {
    return i18n.changeLanguage(code);
  };

  const editPost = () => {
    props.navigation.navigate('EditPost', { post: post });
    return;
  };

  const deleteClicked = () => {
    setDeleteVisible(true);
    setOpen(false);
    setModalShown(true);
    dispatch(
      toggleModal(
        true,
        ModalTexts.MYPOSTS_SCREEN_SURE_DELETE_TITLE,
        ModalTexts.MYPOSTS_SCREEN_SURE_DELETE_DESCRIPTION,
        false,
      ),
    );
  };

  const onDeletePost = async () => {
    try {
      setModalShown(false);
      dispatch(toggleModal(false, '', ''));
      setLoading(true);
      await deletePost(post.id);
      setLoading(false);
      setDeleteVisible(false);
      dispatch(toggleReaload(!reload));
      props.navigation.pop();
    } catch (e) {
      console.log('load error', e);
      setModalShown(false);
      dispatch(toggleModal(false, '', ''));
      setLoading(false);
      setDeleteVisible(false);
    }
  };

  const makeCall = () => {
    console.log('+++++++++callNumber ', post.user.phone);
    let phoneNumber = post.user.phone;
    if (Platform.OS == 'android') {
      phoneNumber = `tel:${post.user.phone}`;
    }
    if (Platform.OS == 'ios') {
      phoneNumber = `telprompt:${post.user.phone}`;
    }
    Linking.canOpenURL(phoneNumber)
      .then(supported => {
        if (!supported) {
          Alert.alert('Number is not available');
        } else {
          return Linking.openURL(phoneNumber);
        }
      })
      .catch(err => console.log(err));
  };

  const openWhatsApp = () => {
    let mobile = post.user.phone;
    console.log('mobile', mobile);
    if (mobile) {
      let url = 'whatsapp://send?text=' + '&phone=20' + post.user.phone;
      Linking.openURL(url)
        .then(data => {
          console.log('WhatsApp Opened successfully ' + data);
        })
        .catch(() => {
          alert('Make sure WhatsApp installed on your device');
        });
    } else {
      alert("there's no phone number");
    }
  };

  if (!post) {
    return <Text>Nothing to show</Text>;
  }
  const DATA = [
    {
      name: 'WHATSAPP',
      iconName: 'whatsapp',
      iconSize: 28,
      press: openWhatsApp,
      isContact:
        typeContactMethod == 'WHATSAPP' || typeContactMethod == 'BOTH'
          ? true
          : false,
    },
    {
      name: 'PHONE',
      iconName: 'phone',
      iconSize: 23,
      press: makeCall,
      isContact:
        typeContactMethod == 'PHONE' || typeContactMethod == 'BOTH'
          ? true
          : false,
    },
  ];

  return (
    <>
      {loading && <LoadingSpinner />}
      <ModalCard
        modalShown={modalShown}
        onBackdropPress={() => setModalShown(false)}
        isBasicModal={deleteVisible ? false : true}
        cancelButton={
          deleteVisible ? ModalTexts.MODAL_CANCEL : ModalTexts.MODAL_OK
        }
        actionButton={deleteVisible ? 'Delete' : null}
        actionButtonFunction={deleteVisible ? onDeletePost : null}
      />
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          backgroundColor: Colors.lightBg,
          position: 'relative',
        }}
      >
        <ScrollView
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{ paddingBottom: 80 }}
        >
          <View style={styles.imageContainer}>
            <Image
              style={styles.image}
              resizeMode="cover"
              source={{ uri: post.image_url }}
              defaultSource={defaultImage}
            />
          </View>
          <View style={styles.productInfo}>
            <View style={styles.postTitleContainer}>
              <Text style={styles.postTitleText}>{post.title}</Text>
            </View>
            <View style={styles.productInfoRaw}>
              <Text>{post.description}</Text>
            </View>
            <Divider
              width={1}
              color={Colors.COLOR_PRIMARY}
              style={styles.divider}
            />
            {!isUserOwnPost && (
              <>
                <View style={styles.productInfoRaw}>
                  <Text>{t(`common:posted_by_label_post_details_screen`)}</Text>
                  <Text>{post?.user?.name}</Text>
                </View>
                <Divider
                  width={1}
                  color={Colors.COLOR_PRIMARY}
                  style={styles.divider}
                />
              </>
            )}
            <View style={styles.productInfoRaw}>
              <Text>{t(`common:post_type_label_post_details_screen`)}</Text>
              <Text>{post.post_type_readable}</Text>
            </View>
            <Divider
              width={1}
              color={Colors.COLOR_PRIMARY}
              style={styles.divider}
            />

            <View style={styles.productInfoRaw}>
              <Text>{t(`common:date_added_label_post_details_screen`)}</Text>
              <Text>{helpers.parseDate(post.created_at)}</Text>
            </View>
            <Divider
              width={1}
              color={Colors.COLOR_PRIMARY}
              style={styles.divider}
            />
            {isUserOwnPost && (
              <>
                <View style={styles.productInfoRaw}>
                  <Text>Status</Text>
                  <Text>{helpers.formatStatusText(post.status)}</Text>
                </View>
                <Divider
                  width={1}
                  color={Colors.COLOR_PRIMARY}
                  style={styles.divider}
                />
              </>
            )}

            {post.post_type_readable === 'SELL' && post.price != 0 && (
              <>
                <View style={styles.productInfoRaw}>
                  <Text>{t(`common:price_label`)}</Text>
                  <Text>
                    <IconMaterial name="currency-gbp" color="gray" size={15} />
                    {post.price}
                  </Text>
                </View>
                <Divider
                  width={1}
                  color={Colors.COLOR_PRIMARY}
                  style={styles.divider}
                />
              </>
            )}
            {post.post_type_readable === 'EXCHANGE' && (
              <>
                <View style={styles.productInfoRaw}>
                  <Text
                    adjustsFontSizeToFit
                    numberOfLines={1}
                    style={{ marginRight: 6 }}
                  >
                    {t(`common:exchange_details_label`)}
                  </Text>
                  <Text
                    adjustsFontSizeToFit
                    numberOfLines={0}
                    style={{ flex: 1, flexWrap: 'wrap', textAlign: 'right' }}
                  >
                    {post.exchange_details}
                  </Text>
                </View>
                <Divider
                  width={1}
                  color={Colors.COLOR_PRIMARY}
                  style={styles.divider}
                />
              </>
            )}

            <View style={styles.productInfoRaw}>
              <Text>{t(`common:contact_method_label`)}</Text>
              <View style={{ flexDirection: 'row' }}>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                  }}
                >
                  {DATA.map(contact => {
                    if (contact.isContact) {
                      return (
                        <TouchableOpacity
                          onPress={isUserOwnPost ? null : contact.press}
                          key={contact.name}
                        >
                          <Icon
                            name={contact.iconName}
                            color={Colors.COLOR_PRIMARY}
                            size={contact.iconSize}
                            style={{ marginLeft: 17 }}
                          />
                        </TouchableOpacity>
                      );
                    }
                  })}
                </View>
              </View>
            </View>
            <Divider
              width={1}
              color={Colors.COLOR_PRIMARY}
              style={styles.divider}
            />
            {post.number_of_papers && (
              <>
                <Divider
                  width={3}
                  color={Colors.COLOR_PRIMARY}
                  style={styles.divider}
                />
                <View style={styles.productInfoRaw}>
                  <Text>Number of papers</Text>
                  <View>
                    <Text style={{ textAlign: 'left', maxWidth: 200, flex: 1 }}>
                      {post.number_of_papers}
                    </Text>
                  </View>
                </View>
                <Divider
                  width={1}
                  color={Colors.COLOR_PRIMARY}
                  style={styles.divider}
                />
              </>
            )}
            <View
              style={{
                ...styles.productInfoRaw,
                justifyContent: 'flex-start',
                alignItems: 'center',
              }}
            >
              <Icon
                name="map-marker-alt"
                size={24}
                color={Colors.COLOR_PRIMARY_GREEN}
              />
              <Text style={{ paddingLeft: 6 }}>{post.pickup_location}</Text>
            </View>
          </View>
          {!isUserOwnPost && post.status == 'ACTIVE' && (
            <BuyButton postId={post.id} />
          )}
        </ScrollView>

        {isUserOwnPost && post.status !== 'SOLD' && (
          <SpeedDial
            transitionDuration={100}
            style={{ paddingBottom: 60 }}
            isOpen={open}
            buttonStyle={{ backgroundColor: Colors.COLOR_PRIMARY }}
            icon={
              <Icon
                name="ellipsis-h"
                adjustsFontSizeToFit
                size={35}
                color="#fff"
              />
            }
            openIcon={{ name: 'close', color: '#fff' }}
            onOpen={() => setOpen(!open)}
            onClose={() => setOpen(!open)}
          >
            {post.status !== 'ACTIVE' && (
              <SpeedDial.Action
                buttonStyle={{ backgroundColor: Colors.COLOR_SECONDARY }}
                icon={{ name: 'edit', color: '#fff' }}
                onPress={editPost}
              />
            )}
            <SpeedDial.Action
              buttonStyle={{ backgroundColor: Colors.DELETE_BUTTON }}
              icon={{ name: 'delete', color: '#fff' }}
              onPress={deleteClicked}
            />
          </SpeedDial>
        )}
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  productInfo: {
    margin: 15,
    backgroundColor: 'white',
    borderRadius: 30,
    shadowColor: 'black',
    shadowOffset: { width: 0, height: 2 },
    elevation: 5,
    shadowOpacity: 0.26,
    padding: 10,
    marginBottom: 30,
  },

  productInfoRaw: {
    margin: 15,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },

  postTitleContainer: {
    margin: 15,
    textAlign: 'center',
  },

  postTitleText: {
    textAlign: 'center',
    marginVertical: 10,
    fontSize: 18,
    fontWeight: 'bold',
  },

  divider: {
    marginHorizontal: 15,
  },

  imageContainer: {
    width: '100%',
    height: 300,
    overflow: 'hidden',
    borderBottomEndRadius: 30,
    borderBottomStartRadius: 30,
  },

  image: {
    width: '100%',
    height: '100%',
  },

  deleteButtonView: {
    marginVertical: 20,
    alignItems: 'center',
  },

  deleteButton: {
    backgroundColor: Colors.DELETE_BUTTON,
  },

  buttonContainer: {
    height: 40,
    width: 120,
    marginHorizontal: 50,
    marginVertical: 10,
  },

  buttonTitle: {
    color: 'white',
    marginHorizontal: 20,
  },
  centered: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default MyPostDetailsScreen;
