import React, { useState, useEffect } from 'react';
import {
  View,
  FlatList,
  StyleSheet,
  Text,
  RefreshControl,
  ActivityIndicator,
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import Post from '../../components/Post';
import { getPosts as getApiPosts } from '../../infrastructure/api/api';
import LoadingSpinner from '../../components/loading/LoadingSpinner';
import { Colors, ModalTexts } from '../../shared/constants';
import ModalCard from '../../components/modal/Modal';
import { toggleModal } from '../../store/actions/modal';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { useScrollToTop } from '@react-navigation/native';
import CustomButtonSecondary from '../../components/custom-button/CustomButton';
import { useTranslation } from 'react-i18next';

const HomeScreen = props => {
  const dispatch = useDispatch();
  const [posts, setPosts] = useState([]);
  const [loading, setLoading] = useState(false);
  const [bottomLoader, setBottomLoader] = useState(false);
  const reload = useSelector(state => state.loading.reload);
  const [refresh, setRefresh] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);
  const [endReached, setEndReached] = useState(false);
  const [refreshing, setRefreshing] = useState(false);
  const [modalShown, setModalShown] = useState(false);
  const ref = React.useRef(null);

  const { i18n, t } = useTranslation();
  const setLanguage = code => {
    return i18n.changeLanguage(code);
  };

  useScrollToTop(ref);

  useEffect(() => {
    if (!endReached) {
      loadPosts(refreshing || bottomLoader ? false : true);
    }
  }, [reload, currentPage, refresh]);

  const loadPosts = async (loadSpinner = true) => {
    try {
      loadSpinner ? setLoading(true) : null;
      const apiPosts = await getApiPosts(currentPage);
      if (currentPage == 1) {
        setPosts(apiPosts.data.data);
      } else {
        apiPosts.data.data.length > 0
          ? setPosts(posts.concat(apiPosts.data.data))
          : setEndReached(true);
      }
      loadSpinner ? setLoading(false) : null;
      setRefreshing(false);
      setBottomLoader(false);
    } catch (error) {
      loadSpinner ? setLoading(false) : null;
      setBottomLoader(false);
      setModalShown(true);
      dispatch(
        toggleModal(true, ModalTexts.EMPTY_FIELD_TITLE, error.data.message),
      );
      setRefreshing(false);
    }
  };

  const onRefresh = () => {
    setRefreshing(true);
    setRefresh(!refresh);
    setCurrentPage(1);
    setEndReached(false);
  };

  const onSelectPost = post => {
    props.navigation.navigate('MyPostDetailsScreen', { post: post });
  };

  const postItem = ({ item }) => {
    return (
      <Post
        title={item.title}
        price={item.price}
        description={item.description}
        imageURL={item.image_url}
        createdAt={item.created_at_readable}
        postOwner={item.user.name}
        postType={item.post_type_readable}
        onSelect={() => onSelectPost(item)}
      />
    );
  };

  const loadMorePosts = () => {
    if (!endReached) {
      setBottomLoader(true);
      setCurrentPage(current => current + 1);
    }
  };

  if (posts.length < 1 && !loading) {
    return (
      <View style={styles.emptyContainer}>
        <View style={styles.iconWithTextsContainer}>
          <Icon name="leaf" size={150} color={Colors.COLOR_PRIMARY} />
          <View style={styles.textContainer}>
            <Text style={styles.titleText}>No Posts Yet</Text>
            <Text style={styles.descriptionText}>
              {t(`common:no_posts_home_screen`)}
            </Text>
          </View>
        </View>
        <CustomButtonSecondary
          title={t(`common:refresh_button`)}
          onPress={loadPosts}
        />
      </View>
    );
  }

  return (
    <>
      {loading && <LoadingSpinner />}
      <ModalCard
        modalShown={modalShown}
        onBackdropPress={() => setModalShown(false)}
        isBasicModal={true}
        cancelButton={ModalTexts.MODAL_OK}
      />
      <View style={styles.mainContainer}>
        <FlatList
          initialNumToRender={2}
          windowSize={4}
          contentContainerStyle={{ paddingBottom: 160 }}
          style={{ flexGrow: 1 }}
          showsVerticalScrollIndicator={false}
          ref={ref}
          data={posts}
          renderItem={postItem}
          onEndReached={!bottomLoader ? loadMorePosts : null}
          onEndReachedThreshold={0}
          refreshControl={
            <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
          }
        />
        {bottomLoader && posts.length >= 10 && (
          <View
            style={{ position: 'absolute', bottom: 100, alignSelf: 'center' }}
          >
            <ActivityIndicator size="large" color={Colors.COLOR_PRIMARY} />
          </View>
        )}
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: Colors.lightBg,
  },
  centered: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  emptyContainer: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: Colors.lightBg,
  },

  iconWithTextsContainer: { alignItems: 'center' },

  textContainer: { marginTop: 20, paddingBottom: 10, alignItems: 'center' },

  titleText: {
    fontSize: 22,
    color: Colors.textThird,
    fontWeight: '700',
  },

  descriptionText: { color: Colors.textThird },
});

export default HomeScreen;
