import React, { useEffect, useState, useCallback } from 'react';
import {
  View,
  FlatList,
  StyleSheet,
  Text,
  RefreshControl,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { useDispatch, useSelector } from 'react-redux';
import Post from '../../components/Post';
import { Colors, ModalTexts } from '../../shared/constants';
import { getMyPosts as getApiMyPosts } from '../../infrastructure/api/api';
import LoadingSpinner from '../../components/loading/LoadingSpinner';
import { toggleModal } from '../../store/actions/modal';
import ModalCard from '../../components/modal/Modal';
import { setNumberOfPosts } from '../../store/actions/auth';
import CustomButtonSecondary from '../../components/custom-button/CustomButton';

const MyPostsScreen = props => {
  const dispatch = useDispatch();
  const [posts, setPosts] = useState([]);
  const [refreshing, setRefreshing] = useState(false);

  const reload = useSelector(state => state.loading.reload);

  const [loading, setLoading] = useState(false);
  const [modalShown, setModalShown] = useState(false);

  const loadPosts = useCallback(
    async (loadSpinner = true) => {
      try {
        loadSpinner ? setLoading(true) : null;
        const posts = await getApiMyPosts();
        setPosts(posts.data.data);
        dispatch(setNumberOfPosts(posts.data.total));
        loadSpinner ? setLoading(false) : null;
      } catch (error) {
        loadSpinner ? setLoading(false) : null;
        setModalShown(true);
        dispatch(
          toggleModal(true, ModalTexts.EMPTY_FIELD_TITLE, error.data.message),
        );
      }
    },
    [dispatch],
  );

  useEffect(() => {
    loadPosts();
  }, [reload]);

  const onRefresh = useCallback(async () => {
    setRefreshing(true);
    await loadPosts(false);
    setRefreshing(false);
  }, []);

  const onSelectPost = post => {
    props.navigation.navigate('MyPostDetailsScreen', { post: post });
  };

  const renderPost = ({ item }) => {
    return (
      <Post
        title={item.title}
        price={item.price}
        description={item.description}
        imageURL={item.image_url}
        createdAt={item.created_at_readable}
        status={item.status}
        postType={item.post_type_readable}
        onSelect={() => {
          onSelectPost(item);
        }}></Post>
    );
  };

  if (posts.length < 1 && loading == false) {
    return (
      <View style={styles.emptyContainer}>
        <View style={styles.iconWithTextsContainer}>
          <Icon name="leaf" size={150} color={Colors.COLOR_PRIMARY} />

          <View style={styles.textContainer}>
            <Text style={styles.titleText}>You Don't Have Any Posts Yet</Text>
            <Text style={styles.descriptionText}>
              When you have any posts, they'll show up here
            </Text>
          </View>
        </View>
        <CustomButtonSecondary title="Refresh" onPress={loadPosts} />
      </View>
    );
  }

  return (
    <>
      {loading && <LoadingSpinner />}
      <ModalCard
        modalShown={modalShown}
        onBackdropPress={() => setModalShown(false)}
        isBasicModal={true}
        cancelButton={ModalTexts.MODAL_OK}
      />

      <View style={styles.mainContainer}>
        <FlatList
          data={posts}
          initialNumToRender={2}
          windowSize={4}
          renderItem={renderPost}
          contentContainerStyle={{ paddingBottom: 70 }}
          refreshControl={
            <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
          }
        />
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  centered: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },

  mainContainer: {
    flex: 1,
    backgroundColor: Colors.lightBg,
  },

  emptyFlatlist: {
    flex: 1,
    alignItems: 'center',
  },

  emptyFlatlistText: {
    color: 'gray',
  },
  emptyContainer: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: Colors.lightBg,
  },

  iconWithTextsContainer: { alignItems: 'center' },

  textContainer: { marginTop: 20, paddingBottom: 10, alignItems: 'center' },

  titleText: {
    fontSize: 22,
    color: Colors.textThird,
    fontWeight: '700',
  },

  descriptionText: { color: Colors.textThird },
});

export default MyPostsScreen;
