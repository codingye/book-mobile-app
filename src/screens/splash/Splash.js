import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import AuthNavigation from '../../navigation/AuthNavigation';
import AppNavigation from '../../navigation/AppNavigation';
import AsyncStorage from '@react-native-async-storage/async-storage';
import * as authActions from '../../store/actions/auth';
import { AsyncStorageKeys, Colors } from '../../shared/constants';
import { Text, View } from 'react-native';

const Splash = () => {
  const dispatch = useDispatch();
  const currentUser = useSelector(state => state.user.currentUser);
  const [isReady , setIsReady] = useState(false)

  console.log('current user app', currentUser);

  useEffect(() => {
    !currentUser ? checkUserToken() : null;
    setTimeout(() => {
      setIsReady(true)
    }, 1000);
  }, []);

  const checkUserToken = async () => {
    let userJSON = await AsyncStorage.getItem(AsyncStorageKeys.USER_KEY);
    let userData = userJSON ? JSON.parse(userJSON) : null;
    if (userData) {
      dispatch(authActions.setCurrentUser(userData));
    }
  };

  return (
    <>
      {!isReady && <View style = {{flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: Colors.COLOR_OFF_WHITE}}>
        <Text style = {{color: Colors.COLOR_PRIMARY_GREEN, fontSize: 24}}>Gonnet</Text>
        <Text style = {{color: Colors.TEXT_GRAY, fontSize: 16, position:'absolute', bottom: 100}}>V1.0</Text>
        </View>}
      {!currentUser && isReady && <AuthNavigation />}
      {currentUser && isReady && <AppNavigation />}
    </>
  );
};

export default Splash;
