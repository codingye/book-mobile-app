import { ADD_POST, DELETE_POST, GET_POSTS } from '../actions/post';

const initialState = {
  availablePosts: [],
  userPosts: [],
};

const getPosts = (state, action) => {
  return { ...state, availablePosts: action.payload };
};

const addPost = (state, action) => {
  return {
    ...state,
    availablePosts: state.availablePosts.concat(action.payload),
    userPosts: state.availablePosts.concat(action.payload),
  };
};

const deletePost = (state, action) => {
  return {
    ...state,
    availablePosts: state.availablePosts.filter(
      post => post.id !== action.postId,
    ),
    userPosts: state.userPosts.filter(post => post.id !== action.postId),
  };
};

const postReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_POSTS:
      return getPosts(state, action);

    case ADD_POST:
      return addPost(state, action);

    case DELETE_POST:
      return deletePost(state, action);
  }
  return state;
};

export default postReducer;
