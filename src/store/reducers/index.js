import { combineReducers } from 'redux';
import postReducer from './post';
import authReducer from './auth';
import loading from './loading';
import modal from './modal';
import notification from './notification';

const rootReducer = combineReducers({
  user: authReducer,
  posts: postReducer,
  loading: loading,
  modal: modal,
  notification: notification
});

export default rootReducer;
