import {
  TOGGLE_LOADING_SPINNER,
  TOGGLE_RELOAD,
} from '../actions/loadingActions';

const initialState = {
  loadingState: false,
  reload: false,
};

const toggleLoading = (state = initialState.loadingState, action) => {
  return { ...state, loadingState: action.payload };
};

const toggleReload = (state = initialState.reload, action) => {
  return { ...state, reload: action.payload };
};

export const loading = (state = initialState, action) => {
  switch (action.type) {
    case TOGGLE_LOADING_SPINNER:
      return toggleLoading(state, action);
    case TOGGLE_RELOAD:
      return toggleReload(state, action);
    default:
      return state;
  }
};

export default loading;
