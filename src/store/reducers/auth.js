import {
  SET_CURRENT_USER,
  SET_NUMBER_OF_POSTS,
  UPDATE_USER_POINTS,
} from '../actions/auth';

const initialState = {
  currentUser: null,
  numberOfPosts: 0,
  points: 0
};

const setCurrentUser = (state, action) => {
  return { ...state, currentUser: action.payload };
};

const updateUserPoints = (state, action) => {
  return { ...state, points: action.payload };
};

const setNumberOfPosts = (state, action) => {
  return { ...state, numberOfPosts: action.payload };
};

const user = (state = initialState, action) => {
  switch (action.type) {
    case SET_CURRENT_USER:
      return setCurrentUser(state, action);
    case SET_NUMBER_OF_POSTS:
      return setNumberOfPosts(state, action);
    case UPDATE_USER_POINTS:
      return updateUserPoints(state, action);
    default:
      return state;
  }
};

export default user;
