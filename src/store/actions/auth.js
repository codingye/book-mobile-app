export const SIGNUP = 'SIGNUP';
export const LOGIN = 'LOGIN';
export const SET_CURRENT_USER = 'SET_CURRENT_USER';
export const SET_NUMBER_OF_POSTS = 'SET_NUMBER_OF_POSTS';
export const UPDATE_USER_POINTS = 'UPDATE_USER_POINTS';

const setCurrentUser = user => {
  return {
    type: SET_CURRENT_USER,
    payload: user,
  };
};

const updateUserPoints = points => {
  return {
    type: UPDATE_USER_POINTS,
    payload: points,
  };
};

const setNumberOfPosts = numOfPosts => {
  return {
    type: SET_NUMBER_OF_POSTS,
    payload: numOfPosts,
  };
};

export { setCurrentUser, setNumberOfPosts, updateUserPoints };
