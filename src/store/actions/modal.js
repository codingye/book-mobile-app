export const TOGGLE_MODAL = 'TOGGLE_MODAL';

const toggleModal = (newState, title, description, isBasic) => {
  return {
    type: TOGGLE_MODAL,
    payload: { newState, title, description, isBasic },
  };
};

export { toggleModal };
