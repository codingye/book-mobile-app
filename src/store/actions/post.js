import {
  getPosts as getApiPosts,
  deletePost as deleteApiPost,
} from '../../infrastructure/api/api';

export const ADD_POST = 'ADD_POST';
export const GET_POSTS = 'GET_POSTS';
export const DELETE_POST = 'DELETE_POST';

export const getPosts = async () => {
  try {
    const posts = await getApiPosts();
    return posts.data.data;
  } catch (error) {
    throw error;
  }
};

export const deletePost = postId => {
  return async dispatch => {
    try {
      await deleteApiPost(postId);
    } catch (e) {
      // helpers.notifyUser(e);
      return;
    }

    return dispatch({ type: DELETE_POST, postId });
  };
};
