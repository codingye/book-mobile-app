export const TOGGLE_LOADING_SPINNER = 'TOGGLE_LOADING_SPINNER';
export const TOGGLE_RELOAD = 'TOGGLE_RELOAD';

const toggleLoading = newState => {
  return {
    type: TOGGLE_LOADING_SPINNER,
    payload: newState,
  };
};

const toggleReaload = newState => {
  return {
    type: TOGGLE_RELOAD,
    payload: newState,
  };
};

export { toggleLoading, toggleReaload };
