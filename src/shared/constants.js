import { Dimensions, StyleSheet } from 'react-native';
import Config from 'react-native-config';

export const AsyncStorageKeys = {
  USER_KEY: '@user_Key',
  USER_POSTS: '@user_posts',
};

export const ScreenDimensions = {
  HEIGHT: Dimensions.get('window').height,
  WIDTH: Dimensions.get('window').width,
};

export const Endpoints = {
  BASE_URL: Config.BASE_URL,
  LOGIN: 'login',
  REGISTER: 'register',
  POSTS: 'posts',
  LOGOUT: 'logout',
  UPLOAD_IMAGE: 'posts/image',
  USERS: 'users',
  USER: 'user',
  POST_TYPES: 'post-types',
  SET_FCM_TOKEN: 'users/setFcmToken',
  DELETE_FCM_TOKEN: 'users/deleteFcmToken',
  NOTIFICATION: 'users/notifications',
  TRANSACTIONS: 'transactions',
  MY_TRANSACTIONS: 'users/transactions',
  CONFIRM_TRANSACTION: 'transactions/confirm',
  LEADERBOARD: 'leaderboard',
};

export const Colors = {
  COLOR_PRIMARY: '#2f783a',
  COLOR_SECONDARY: '#065c38',
  COLOR_OFF_WHITE: '#f7f7f7',
  COLOR_GRAY: '#d1d1d1',
  COLOR_PRIMARY_GREEN: '#2f783a',
  COLOR_SECONDARY_GREEN: '#cadecd',
  COLOR_GREEN_INACTIVE: '#679e6f',
  COLOR_DANGER: '#df4759',
  COLOR_WARNING: '#edba2d',
  TEXT_GRAY: '#9c9c9c',
  GOOGLE_BUTTON: '#4287f5',
  ADD_BUTTON: '#22873d',
  DELETE_BUTTON: '#c91212',
  SUCCESS_BUTTON: '#5cb85c',
  CHECKMARK: '#0083c9',
  darkBg: '#222',
  lightBg: 'white',
  darkHL: '#666',
  lightHL: '#888',
  pink: '#ea3372',
  orange: '#F97878',
  text: '#fff',
  textSec: '#aaa',
  textThird: '#969997',
  whiteText: 'white',
  blackText: 'black',
  tabbarInactiveTint: '#9c9c9c',
  divider: '#f7f7f7',
  colorPrimaryShadow: 'rgba(0, 151, 167, 0.4)',
  colorPrimaryInActive: '#63b6bf',
  textLightGray: '#f0f0f0',
};

export const LoaderDimensions = {
  BORDER_WIDTH: 6,
  SIZE: 80,
};

export const ModalTexts = {
  APP_VERSION_TITLE: 'App Version',
  APP_VERSION_DESCRIPTION: 'v1.0',
  EMPTY_FIELD_TITLE: 'Error',
  EMPTY_FIELD_DESCRIPTION: 'All fields are required.',
  WRONG_USERNAME_TITLE: 'Invalid Credentials',
  WRONG_USERNAME_DESCRIPTION:
    'Please check that you have entered the right username and password.',
  UNAUTHENTICATED_TITLE: 'Authentication Error',
  UNAUTHENTICATED_DESCRIPTION: 'Please sign in again.',
  GENERAL_ERROR_TITLE: 'Ooops!',
  GENERAL_ERROR_DESCRIPTION: 'Something went wrong. Please try again.',
  FORGET_PASSWORD_EMPTY_FIELD_TITLE: 'This field is required',
  FORGET_PASSWORD_EMPTY_FIELD_DESCRIPTION: 'Please fill in your username.',
  FORGET_PASSWORD_WRONG_USERNAME_TITLE: 'Something went wrong...',
  FORGET_PASSWORD_WRONG_USERNAME_DESCRIPTION:
    'The provided username is invalid.',
  FORGET_PASSWORD_COMPLETION_TITLE: 'Email sent!',
  FORGET_PASSWORD_COMPLETION_DESCRIPTION:
    'Please checkout your mail inbox for the instructions.',
  USER_FAILED_ACTION: 'Failed!',
  USER_ALREADY_EXISTS_DESCRIPTION: 'User already exists.',
  MYPOSTS_SCREEN_SURE_DELETE_TITLE: 'Are you sure?',
  MYPOSTS_SCREEN_SURE_DELETE_DESCRIPTION:
    "You won't be able to get this post again.",
  QR_CODE_CONFIRM_TITLE: 'Successful Transaction',
  QR_CODE_CONFIRM_DESCRIPTION: 'Congrats! You have earned more points.',
  MODAL_OK: 'OK',
  MODAL_CANCEL: 'Cancel',
  FORM_ERROR: 'Please enter the fields in your form correctly',
};

export const defaultImage = require('../assets/imagePlaceholder.jpg');

export const generalStyles = StyleSheet.create({
  rowBetween: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  center: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    color: Colors.text,
    fontSize: 30,
  },
  subtitle: {
    fontWeight: '600',
    textTransform: 'uppercase',
    color: 'rgba(255, 255, 255, 0.6)',
    fontSize: 15,
    letterSpacing: 1,
  },
  rowCenter: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  button: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.pink,
    borderRadius: 100,
  },
  sectionContainer: {
    paddingVertical: 24,
    paddingHorizontal: 24,
    marginBottom: 8,
    backgroundColor: Colors.lightBg,
  },
  sectionTitle: {
    fontWeight: '700',
    color: Colors.text,
    fontSize: 15,
  },
  smallText: {
    fontSize: 12,
    fontWeight: '800',
    color: Colors.text,
  },
});
