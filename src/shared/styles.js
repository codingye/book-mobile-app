import { StyleSheet } from 'react-native';
import { Colors } from './constants';

//Global in app styles file

export const buttonStyles = StyleSheet.create({
  deleteButton: {
    backgroundColor: Colors.DELETE_BUTTON,
    padding: 10,
    paddingHorizontal: 20,
  },

  cancelButton: {
    backgroundColor: Colors.COLOR_PRIMARY,
    padding: 10,
    paddingHorizontal: 20,
  },
});
