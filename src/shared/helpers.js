const getUserNameNickName = username => {
  const splitted = username.split(' ');
  let nickname = splitted.map(name => name[0].toUpperCase()).join('');
  return nickname.substring(0, 2);
};

const parseDate = dateString => {
  const date = new Date(dateString);
  const day = String(
    date.toLocaleString('en-US', {
      // weekday: 'short',
      day: 'numeric',
      year: 'numeric',
      month: 'long',
      hour: 'numeric',
      minute: 'numeric',
    }),
  );

  return day;
};
const parseUserProfileDate = dateString => {
  const date = new Date(dateString);
  const month = String(
    date.toLocaleString('en-US', {
      year: 'numeric',
      month: 'long',
    }),
  );

  return month;
};

const getStatusIcon = status => {
  switch (status) {
    case 'SOLD':
      return 'check-circle';
    case 'INREVIEW':
      return 'spinner';
    case 'ACTIVE':
      return 'check-circle';
    case 'REJECTED':
      return 'ban';
    default:
      return 'question';
  }
};

const formatStatusText = status => {
  switch (status) {
    case 'SOLD':
      return 'Sold';
    case 'INREVIEW':
      return 'In Review';
    case 'ACTIVE':
      return 'Active';
    case 'REJECTED':
      return 'Rejected';
    default:
      return 'Unknown';
  }
};

export default {
  parseDate,
  parseUserProfileDate,
  getStatusIcon,
  formatStatusText,
  getUserNameNickName,
};
