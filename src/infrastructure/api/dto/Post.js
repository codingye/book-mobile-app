export default class PostDto {
  constructor(
    userId,
    title,
    imageUrl,
    description,
    pickupLocation,
    postTypeId,
    price,
    exchangeDetails,
    contactMethod,
    status,
  ) {
    this.user_id = userId;
    this.title = title;
    this.image_url = imageUrl;
    this.description = description;
    this.pickup_location = pickupLocation;
    this.post_type_id = postTypeId;
    this.contact_method = contactMethod;
    this.status = status;
    this.exchange_details = exchangeDetails;
    this.price = price;
  }
}
