import axios from 'axios';
import * as AxiosLogger from 'axios-logger';
import Config from 'react-native-config';
import { Endpoints } from '../../shared/constants';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { AsyncStorageKeys } from '../../shared/constants';
import store from '../../store';
import * as authActions from '../../store/actions/auth';

const instance = axios.create({
  baseURL: Config.BASE_URL,
  defaultInterceptors: true,
});

instance.interceptors.request.use(request => {
  let reduxState = store.getState();
  console.log('reduxState.user.currentUser', reduxState.user.currentUser);

  if (reduxState?.user?.currentUser) {
    request.headers['Authorization'] =
      'Bearer ' + reduxState.user.currentUser.token;
  }
  console.log('request sent is', request);
  return AxiosLogger.requestLogger(request);
});

instance.interceptors.response.use(
  response => {
    console.log('response is ', response);

    return AxiosLogger.responseLogger(response);
  },
  async error => {
    console.error('error api', error?.response);
    if (error?.response?.status == '404') {
      // helpers.notifyUser('Error, Not Found');
    }
    if (error?.response?.status == '401') {
      store.dispatch(authActions.setCurrentUser(null));
      await AsyncStorage.removeItem(AsyncStorageKeys.USER_KEY);
    }
    return Promise.reject(error?.response);
  },
);

const RequestType = {
  GET: 'GET',
  POST: 'POST',
  PUT: 'PUT',
  DELETE: 'DELETE',
};

function request(type, url, body = {}, headers = {}) {
  const config = {
    onUploadProgress: function (progressEvent) {
      var percentCompleted = Math.round(
        (progressEvent.loaded * 100) / progressEvent.total,
      );
      console.log('percentCompleted', percentCompleted);
    },
  };
  switch (type) {
    case RequestType.GET:
      return instance.get(url, { params: body });
    case RequestType.POST:
      return instance.post(url, body, headers, config);
    case RequestType.PUT:
      return instance.put(url, body, headers, config);
    case RequestType.DELETE:
      return instance.delete(url);
  }
}

export function register(name, email, password, phone) {
  return request(RequestType.POST, Endpoints.REGISTER, {
    name: name,
    email: email,
    password: password,
    phone: phone,
  });
}

export function login(email, password) {
  return request(RequestType.POST, Endpoints.LOGIN, {
    email: email,
    password: password,
  });
}

export function getUser() {
  return request(RequestType.GET, Endpoints.USER);
}

export function getPosts(page = 1) {
  return request(RequestType.GET, Endpoints.POSTS, { page: page });
}

export function getMyPosts() {
  const userId = store.getState().user.currentUser.id;
  return request(RequestType.GET, `${Endpoints.USERS}/${Endpoints.POSTS}`);
}

export function searchPosts(term) {
  return request(RequestType.GET, Endpoints.POSTS, { search: term });
}

export function addPost(postDto) {
  return request(RequestType.POST, Endpoints.POSTS, postDto);
}

export function uploadImage(image) {
  return request(RequestType.POST, Endpoints.UPLOAD_IMAGE, image, {
    headers: { 'Content-Type': 'multipart/form-data' },
    transformRequest: (data, headers) => {
      return image;
    },
  });
}
export function getPostTypes() {
  return request(RequestType.GET, Endpoints.POST_TYPES);
}
export function getSinglePost(postId) {
  return request(RequestType.GET, Endpoints.POSTS + `/${postId}`);
}
export function editPost(postId, data) {
  return request(RequestType.PUT, `${Endpoints.POSTS}/${postId}`, data);
}
export function deletePost(postId) {
  return request(RequestType.DELETE, Endpoints.POSTS + `/${postId}`);
}

export function setFcmToken(fcmToken) {
  return request(RequestType.POST, Endpoints.SET_FCM_TOKEN, {
    token: fcmToken,
  });
}

export function deleteFcmToken(fcmToken) {
  return request(RequestType.POST, Endpoints.DELETE_FCM_TOKEN, {
    token: fcmToken,
  });
}

export function getNotifications() {
  return request(RequestType.GET, Endpoints.NOTIFICATION);
}

export function startTransaction(postId) {
  return request(RequestType.POST, Endpoints.TRANSACTIONS, { post_id: postId });
}

export function confirmTransaction(transactionId, code) {
  return request(RequestType.POST, Endpoints.CONFIRM_TRANSACTION, {
    transaction_id: transactionId,
    code,
  });
}

export function getAllTransactions() {
  return request(RequestType.GET, Endpoints.MY_TRANSACTIONS);
}

export function getLeaderboardUsers() {
  return request(RequestType.GET, Endpoints.LEADERBOARD);
}
export function logout() {
  return request(RequestType.POST, Endpoints.LOGOUT);
}
