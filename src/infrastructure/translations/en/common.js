export default {
  login_screen_welcome: 'Welcome back',
  login_description_login_screen: 'Login to your account',
  email_placeholder: 'Email',
  password_placeholder: 'Password',
  login_button_login_screen: 'Login',
  register_redirect_login_screen: "Don't have an account yet?",

  email_invalid_error_login_register_screen: 'Email is invalid',
  email_required_error_login_register_screen: 'Email is required',
  password_short_error_login_register_screen: 'Password is too short',
  password_required_error_login_register_screen: 'password is required',

  register_header_register_screen: 'Register',
  register_description_register_screen: 'Create your account',
  name_placeholder: 'Full Name',
  phone_number_placeholder: 'Phone Number',
  register_button_register_screen: 'Register',
  login_redirect_register_screen: 'Do you already have an account?',
  username_chars_error_register_screen:
    'Username should be at least 5 characters',
  username_long_error_register_screen: 'Username is too long!',
  username_required_error_register_screen: 'Username is required!',
  phone_number_invalid_error_register_screen: 'Please enter valid number.',
  phone_number_required_error_register_screen: 'Phone number is required',

  posted_by_label_post_details_screen: 'Posted by',
  post_type_label_post_details_screen: 'Ad type',
  date_added_label_post_details_screen: 'Added on',
  contact_method_label_post_details_screen: 'Contact via',
  get_now_button_post_details_screen: 'Get QR code',
  scan_title_post_details_screen: 'let the buyer scan',
  scan_description_post_details_screen:
    'The transaction will start automatically',

  title_label: 'Title',
  title_placeholder: 'Enter your Ad title',
  description_label: 'Description',
  description_placeholder: 'Enter your Ad description',
  post_type_label: 'Ad type',
  post_type_sell_label: 'SELL',
  post_type_exchange_label: 'EXCHANGE',
  post_type_donate_label: 'DONATE',
  price_label: 'Price',
  price_placeholder: 'Enter your Ad price',
  exchange_details_label: 'Exchange details',
  exchange_details_placeholder: 'Enter exchange detials',
  uploade_picture_label: 'Upload picture',
  upload_button: 'Upload',
  contact_method_label: 'Contact via',
  contact_method_whatsApp_label: 'WhatsApp',
  contact_method_phone_label: 'Phone',
  contact_method_both_label: 'Both',
  location_label: 'Pickup location',
  location_placeholder: 'Enter your pickup location',
  post_button: 'Post',
  field_is_required: 'This field is required, fill it',
  minimum_length_field_add_post: 'The minimum length is 5 characters',

  image_support:
    'We do not support this extension\nPlease\
    use one of the following: jpeg,png,jpg,gif,svg',

  my_posts_header_my_posts_screen: 'My Ads',
  notifications_header_notifications_screen: 'Notifications',
  home_header_home_screen: 'HOME',
  add_post_header: 'Add Post',
  edit_post_header: 'Edit Post',
  scan_code_qr_header: 'Scan QR Code',
  all_transactions_header: 'All Transactions',

  sold_label_history_screen: 'Sold',
  bought_label_history_screen: 'Bought',
  no_transaction_title_history_screen: 'No Transactions Yet',
  no_transaction_description_history_screen:
    "When you complete transactions, they'll show up here",

  refresh_button: 'Refresh',

  total_label_settings_screen: 'Total',
  profile_label_settings_screen: 'Profile',
  scan_label_settings_screen: 'Scan',
  history_label_settings_screen: 'History',
  logout_label_settings_screen: 'Logout',
  points_label_settings_screen: 'point',
  language_button_settings_screen: 'العربية',
  language_settings_screen: 'ar',

  count_posts_profile_screen: 'Posts',
  joined_since_profile_screen: 'Joined Since',

  no_posts_home_screen: "When there's active posts, they'll show up here",

  no_notification_title_notification_screen: 'No Notifications Yet',
  no_notification_description_notification_screen:
    "When you get notifications, they'll show up here",

  leaderboard_label: 'Leaderboard',
};
