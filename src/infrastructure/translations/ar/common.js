export default {
  login_screen_welcome: 'مرحبا بك',
  login_screen_description: 'تسجيل الدخول',
  login_description_login_screen: 'تسجيل الدخول الي حسابك',
  email_placeholder: 'البريد الالكتروني',
  password_placeholder: 'كلمة المرور',
  login_button_login_screen: 'تسجيل الدخول',
  register_redirect_login_screen: 'أليس لديك حساب؟',

  email_required_error_login_register_screen: 'يرجي إدخال البريد الالكتروني',
  email_invalid_error_login_register_screen: 'البريد الالكتروني غير صحيح',
  password_short_error_login_register_screen: 'كلمة السر قصيرة',
  password_long_error_login_register_screen: 'كلمة السر طويلة',
  password_required_error_login_register_screen: 'يرجي إدخال كلمة السر',

  register_header_register_screen: 'إنشاء حساب',
  register_description_register_screen: 'أنشئ حساب جديد',
  name_placeholder: 'الأسم كامل',
  phone_number_placeholder: 'رقم الهاتف',
  register_button_register_screen: 'إنشاء حساب',
  login_redirect_register_screen: 'هل لديك حساب بالفعل؟',
  username_chars_error_register_screen:
    'يجب ان يتكون اسم المستخدم من 5 حروف علي الأقل',
  username_long_error_register_screen: 'اسم المستخدم طويل',
  username_required_error_register_screen: 'يرجي إدخال اسم المستخدم',
  phone_number_invalid_error_register_screen: 'يرجي ادخال رقم الهاتف بشكل صحيح',
  phone_number_required_error_register_screen: 'يرجي إدخال رقم الهاتف',

  posted_by_label_post_details_screen: 'تم نشرها من قبل',
  post_type_label_post_details_screen: 'نوع المنشور',
  date_added_label_post_details_screen: 'تاريخ النشر',
  contact_method_label_post_details_screen: 'التواصل من خلال',
  get_now_button_post_details_screen: 'امسح قارئ الرمز',
  scan_title_post_details_screen: 'دع المشتري يفحص قارئ الرمز',
  scan_description_post_details_screen: 'ستبدأ العملية تلقائيًا',

  title_label: 'عنوان الكتاب',
  title_placeholder: 'ادخل عنوان كتابك',
  description_label: 'الوصف',
  description_placeholder: 'ادخل وصف تلخيصي لكتابك',
  post_type_label: 'نوع المنشور',
  post_type_sell_label: 'بيع',
  post_type_exchange_label: 'تبادل',
  post_type_donate_label: 'تبرع',
  price_label: 'السعر',
  price_placeholder: 'ادخل السعر',
  exchange_details_label: 'تفاصيل التبادل',
  exchange_details_placeholder: 'ادخل ملخص ما تفضل ان تتبادل به من كتب',
  uploade_picture_label: 'تحميل صورة',
  upload_button: 'تحميل',
  contact_method_label: 'التواصل عبر',
  contact_method_whatsApp_label: 'الواتساب',
  contact_method_phone_label: 'الهاتف',
  contact_method_both_label: 'كلاهما',
  location_label: 'العنوان',
  location_placeholder: 'ادخل عنوان الالتقاء',
  post_button: 'إضافة منشور',
  field_is_required: 'هذا مطلوب, يرجي ملأه',
  minimum_length_field_add_post: 'اقل عدد من الاحرف التي يمكن ادخاله خمس احرف',

  image_support:
    ':لا ندعم امتداد هذه الصوره, يرجي اختيار امتداد من الآتي\
    jpeg,png,jpg,gif,svg',

  home_header_home_screen: 'الصفحه الرئيسية',
  my_posts_header_my_posts_screen: 'منشوراتي',
  notifications_header_notifications_screen: 'إشعارات',
  add_post_header: 'إضافة منشور',
  edit_post_header: 'تعديل المنشور',
  scan_code_qr_header: 'مسح كود QR',
  all_transactions_header: 'كل المعاملات',

  sold_label_history_screen: 'تم بيعه',
  bought_label_history_screen: 'تم شراؤه',
  no_transaction_title_history_screen: 'لا توجد معاملات حتى الآن',
  no_transaction_description_history_screen: 'عند إتمام المعاملات ، ستظهر هنا',

  refresh_button: 'تحديث',

  total_label_settings_screen: 'المجموع',
  points_label_settings_screen: 'نقطة',
  profile_label_settings_screen: 'الملف الشخصي',
  scan_label_settings_screen: 'مسح',
  history_label_settings_screen: 'السجل',
  logout_label_settings_screen: 'تسجيل الخروج',
  language_button_settings_screen: 'English',
  language_settings_screen: 'en',

  count_posts_profile_screen: 'منشورات',
  joined_since_profile_screen: 'منضم منذ',

  no_posts_home_screen: 'لا يوجد منشورات الآن, ستظهر تلقائيا',

  no_notification_title_notification_screen: 'لا توجد إشعارات',
  no_notification_description_notification_screen:
    'عندما تتلقى إشعارات ، ستظهر هنا',

  leaderboard_label: 'قائمة المتصدرين',
};
