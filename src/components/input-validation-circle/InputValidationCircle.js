import { View } from 'react-native';
import React from 'react';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { Colors } from '../../shared/constants';

const InputValidationCircle = props => {
  return (
    <View
      style={{
        height: 20,
        width: 20,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
        backgroundColor:
          props.error && props.value.trim() != ''
            ? Colors.COLOR_DANGER
            : props.value.trim() != ''
            ? Colors.SUCCESS_BUTTON
            : 'rgba(0,0,0,0)',
        position: 'absolute',
        right: 10,
        top: 15,
      }}
    >
      {props.error && props.value.trim() != '' ? (
        <Icon name="times" size={12} color={Colors.COLOR_OFF_WHITE} />
      ) : props.value.trim() != '' ? (
        <Icon name="check" size={10} color={Colors.COLOR_OFF_WHITE} />
      ) : null}
    </View>
  );
};

export default InputValidationCircle;
