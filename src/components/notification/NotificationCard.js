import React, { useEffect, useRef } from 'react';
import { View, Text, TouchableOpacity, Animated, Easing } from 'react-native';
import { styles } from './styles';

const NotificationCard = props => {
  const opacity = useRef(new Animated.Value(0)).current;
  const scale = useRef(new Animated.Value(0.3)).current;

  useEffect(() => {
    Animated.timing(opacity, {
      toValue: 1,
      duration: 300,
      easing: Easing.inOut(Easing.ease),
      useNativeDriver: true,
    }).start();
    Animated.timing(scale, {
      toValue: 1,
      duration: 150,
      easing: Easing.inOut(Easing.ease),
      useNativeDriver: true,
    }).start();
  }, []);

  return (
    <Animated.View
      style={[styles.container, { opacity: opacity, transform: [{ scale }] }]}
    >
      <TouchableOpacity disabled={props.isDisabled} onPress={props.onClick}>
        <View style={styles.titleAndDateContainer}>
          <Text
            style={styles.titleLabel}
            numberOfLines={2}
            adjustsFontSizeToFit
          >
            {props.title}
          </Text>
          <Text numberOfLines={1} adjustsFontSizeToFit style={styles.dateLabel}>
            {props.date}
          </Text>
        </View>
        <View style={styles.bodyContainer}>
          <Text numberOfLines={2} adjustsFontSizeToFit style={styles.bodyLabel}>
            {props.body}
          </Text>
        </View>
      </TouchableOpacity>
    </Animated.View>
  );
};

export default NotificationCard;
