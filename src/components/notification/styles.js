import { StyleSheet } from 'react-native';
import { Colors } from '../../shared/constants';

export const styles = StyleSheet.create({
  container: {
    margin: 10,
    marginHorizontal: 15,
    backgroundColor: Colors.lightBg,
    borderRadius: 15,
    shadowColor: 'black',
    shadowOffset: { width: 3, height: 8 },
    shadowRadius: 6,
    shadowOpacity: 0.28,
    paddingHorizontal: 10,
    elevation: 5,
  },

  titleAndDateContainer: {
    paddingTop: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },

  titleLabel: {
    fontWeight: 'bold',
    fontSize: 16,
    color: Colors.COLOR_PRIMARY,
    marginRight: 4,
  },

  bodyContainer: {
    marginVertical: 25,
  },

  bodyLabel: {
    color: 'rgba(1,1,1,0.6)',
  },

  dateLabel: {
    fontSize: 12,
    color: Colors.textThird,
    flex: 1,
    textAlign: 'right',
  },
});
