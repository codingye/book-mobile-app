import React from 'react';
import Iconicons from 'react-native-vector-icons/Ionicons';
import { HeaderButton } from 'react-navigation-header-buttons';

export default CustomHeaderButton = props => {
  return <HeaderButton {...props} IconComponent={Iconicons} iconSize={23} />;
};
