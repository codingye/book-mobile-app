import { StyleSheet } from 'react-native';
import { Colors } from '../../shared/constants';

export const fillLoadingSpinner = StyleSheet.absoluteFill;

export const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    alignItems: 'center',
    width: '95%',
    height: '25%',
    borderRadius: 15,
    shadowColor: 'black',
    shadowOffset: { width: 3, height: 8 },
    shadowRadius: 8,
    shadowOpacity: 0.28,
    elevation: 5,
    alignSelf: 'center',
    justifyContent: 'space-between',
  },

  modalTitleAndDescriptionContainer: {
    width: '100%',
    height: '75%',
    padding: 10,
    paddingHorizontal: 20,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'space-evenly',
  },

  modalTitle: {
    fontSize: 23,
    fontWeight: '600',
    color: Colors.COLOR_PRIMARY,
    fontFamily: 'Poppins-Regular',
  },

  modalDescription: {
    color: 'gray',
    fontSize: 16,
    textAlign: 'center',
    fontFamily: 'Poppins-Regular',
  },

  modalButtonsContainer: {
    borderTopWidth: 1,
    borderTopColor: '#e0e0e0',
    flexDirection: 'row',
    width: '100%',
  },

  modalButtonContainer: {},

  modalButtonText: {
    fontSize: 18,
    fontWeight: '500',
    padding: 9,
    borderRadius: 20,
    color: 'gray',
    alignSelf: 'center',
    fontFamily: 'Poppins-Regular',
  },
});
