import React from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import { useDispatch } from 'react-redux';
import { styles } from './styles';
import { useSelector } from 'react-redux';
import Modal from 'react-native-modal';
import { toggleModal } from '../../store/actions/modal';
import { Colors, ModalTexts } from '../../shared/constants';

const ModalCard = props => {
  const modalState = useSelector(state => state.modal);

  return (
    <Modal
      isVisible={props.modalShown}
      onBackdropPress={props.onBackdropPress}
      backdropOpacity={Platform.OS === 'android' ? 0.4 : 0.7}
      backdropTransitionOutTiming={0}
      hideModalContentWhileAnimating
    >
      <View style={styles.container}>
        <View style={styles.modalTitleAndDescriptionContainer}>
          <Text style={styles.modalTitle}>{modalState?.modalTitle}</Text>
          <Text style={styles.modalDescription}>
            {modalState?.modalDescription}
          </Text>
        </View>
        <View style={styles.modalButtonsContainer}>
          <TouchableOpacity
            style={{ flexGrow: 1 }}
            onPress={props.onBackdropPress}
          >
            <View style={[styles.modalButtonContainer]}>
              <Text style={styles.modalButtonText}>
                {modalState?.isBasic ? ModalTexts.MODAL_OK : props.cancelButton}
              </Text>
            </View>
          </TouchableOpacity>
          {!props.isBasicModal && !modalState?.isBasic && (
            <TouchableOpacity
              style={{ flexGrow: 1 }}
              onPress={props.actionButtonFunction}
            >
              <View style={[styles.modalButtonContainer]}>
                <Text
                  style={[
                    styles.modalButtonText,
                    { color: Colors.DELETE_BUTTON },
                  ]}
                >
                  {props.actionButton}
                </Text>
              </View>
            </TouchableOpacity>
          )}
        </View>
      </View>
    </Modal>
  );
};

export default ModalCard;
