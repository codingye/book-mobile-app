import { StyleSheet } from 'react-native';
import { Colors } from '../../shared/constants';

export const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.COLOR_PRIMARY_GREEN,
    height: 42,
    margin: 20,
    justifyContent: 'center',
    borderRadius: 20,
    shadowColor: Colors.COLOR_PRIMARY_GREEN,
    shadowOffset: { width: 1, height: 8 },
    shadowRadius: 6,
    shadowOpacity: 0.24,
    elevation: 7,
  },
  text: {
    alignSelf: 'center',
    color: Colors.whiteText,
    fontSize: 18,
    fontWeight: '500',
  },
});
