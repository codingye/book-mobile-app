import React from 'react';
import { Text, TouchableOpacity } from 'react-native';
import { styles } from './styles';

const CustomButtonSecondary = props => {
  return (
    <TouchableOpacity onPress={props.onPress} style={styles.container}>
      <Text style={styles.text}>{props.title}</Text>
    </TouchableOpacity>
  );
};

export default CustomButtonSecondary;
