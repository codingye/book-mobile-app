import { StyleSheet } from 'react-native';
import React from 'react';
import { Button } from 'react-native-elements';

const AppButton = props => {
  return (
    <Button
      {...props}
      title={props.title}
      buttonStyle={props.buttonStyle}
      containerStyle={{
        ...styles.buttonContainer,
        ...props.containerCustomStyle,
      }}
      titleStyle={{ ...styles.buttonTitle, ...props.titleCustomStyle }}
      onPress={props.onPress}
    />
  );
};

const styles = StyleSheet.create({
  deleteButtonView: {
    marginVertical: 20,
    alignItems: 'center',
  },

  buttonContainer: {
    height: 40,
    width: 120,
    marginHorizontal: 50,
    marginVertical: 10,
  },

  buttonTitle: {
    color: 'white',
    marginHorizontal: 20,
  },
});

export default AppButton;
