import { View } from 'react-native';
import React from 'react';
import { Colors } from '../../shared/constants';

const Divider = () => {
  return (
    <View
      style={{ height: 3, borderRadius: 25, backgroundColor: Colors.divider }}
    />
  );
};

export default Divider;
