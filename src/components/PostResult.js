import React from 'react';
import { View, StyleSheet, TouchableOpacity, Image, Text } from 'react-native';
import EvilIcon from 'react-native-vector-icons/EvilIcons';
import { defaultImage, Colors, ScreenDimensions } from '../shared/constants';

const PostResult = props => {
  return (
    <View>
      <TouchableOpacity onPress={props.onSelect} style={styles.postContainer}>
        <View style={styles.imageContainer}>
          <Image
            style={styles.image}
            source={{ uri: props.imageURL }}
            defaultSource={defaultImage}
          />
        </View>

        <View style={{ width: '70%', paddingLeft: 10 }}>
          <Text
            numberOfLines={2}
            adjustsFontSizeToFit
            style={{
              color: Colors.COLOR_PRIMARY_GREEN,
              fontWeight: 'bold',
              fontSize: 18,
              marginBottom: 1,
            }}>
            {props.title}
          </Text>
          <Text
            numberOfLines={1}
            ellipsizeMode = 'tail'
            style={{ color: Colors.COLOR_GREEN_INACTIVE }}>
            {props.description}
          </Text>
          <View
            style={{
              marginTop: 1,
              alignItems: 'flex-start',
            }}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                marginRight: 16,
                marginLeft: -3.5,
                marginBottom: 1,
              }}>
              <EvilIcon name="user" size={22} color={Colors.COLOR_PRIMARY} />
              <Text
                style={{ color: Colors.COLOR_GREEN_INACTIVE, marginLeft: 2 }}>
                {props.postOwner}
              </Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                marginRight: 16,
                marginLeft: -3.5,
                marginBottom: 1,
              }}>
              <EvilIcon name="clock" size={22} color={Colors.COLOR_PRIMARY} />
              <Text
                style={{ color: Colors.COLOR_GREEN_INACTIVE, marginLeft: 2 }}>
                {props.createdAt}
              </Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                marginRight: 16,
                marginLeft: -3.5,
              }}>
              <Text
                style={{ color: Colors.COLOR_PRIMARY_GREEN, marginLeft: 2}}>
                {props.postType}
              </Text>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  postContainer: {
    backgroundColor: 'white',
    flexDirection: 'row',
    borderRadius: 20,
    shadowColor: 'black',
    shadowOffset: { width: 0, height: 2 },
    elevation: 5,
    shadowOpacity: 0.26,
    padding: 10,
    marginVertical: 4,
    alignSelf: 'center',
    marginHorizontal: 6,
    height: ScreenDimensions.HEIGHT / 5.3,
  },

  image: {
    width: '100%',
    height: '100%',
  },

  imageContainer: {
    width: '30%',
    height: '100%',
    overflow: 'hidden',
    borderRadius: 10,
  },
});

export default PostResult;
