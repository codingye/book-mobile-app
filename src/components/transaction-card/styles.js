import { StyleSheet } from 'react-native';
import { Colors } from '../../shared/constants';

export const styles = StyleSheet.create({
  container: {
    margin: 10,
    marginHorizontal: 15,
    backgroundColor: Colors.lightBg,
    borderRadius: 15,
    shadowColor: 'black',
    shadowOffset: { width: 3, height: 8 },
    shadowRadius: 6,
    shadowOpacity: 0.28,
    paddingHorizontal: 15,
    elevation: 5,
    paddingVertical: 10,
  },
});
