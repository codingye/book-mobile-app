import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
const CustomButton = props => {
  return (
    <View style={{ ...styles.buttonContainer, ...props.style }}>
      <TouchableOpacity style={{ ...styles.button, ...props.style }}>
        <View style={{ flexDirection: 'row' }}>
          {props.children}
          <Text style={{ ...styles.buttonText, ...props.style }}>
            {props.clickText}
          </Text>
        </View>
      </TouchableOpacity>
    </View>
  );
};
const styles = StyleSheet.create({
  buttonContainer: {
    marginTop: 13,
  },
  button: {
    alignItems: 'center',
    height: 40,
    maxHeight: 60,
    justifyContent: 'center',
  },
  buttonText: {
    color: 'white',
    fontSize: 19,
    fontFamily: 'Arial',
    fontWeight: 'bold',
    marginStart: 10,
  },
});

export default CustomButton;
