import { View, Text, Image, StyleSheet, Animated, Button } from 'react-native';
import React, { useRef, useState, useEffect, usePrevious } from 'react';

const SuccessfulUploaded = props => {
  const [displayImage, setDisplayedImage] = useState(false);
  const [displayMessage, setDisplayedMessage] = useState(true);
  const timerRef = useRef(null);
  const fadeAnim = useRef(new Animated.Value(0)).current;

  const closeImage = () => {
    Animated.timing(fadeAnim, {
      toValue: 0,
      useNativeDriver: true,
      duration: 1000,
    }).start(() => {
      setDisplayedImage(false);
    });
  };

  const showImage = () => {
    Animated.timing(fadeAnim, {
      toValue: 1,
      useNativeDriver: true,
      duration: 2000,
    }).start();
    setDisplayedImage(true);
  };

  const sendMessage = () => {
    timerRef.current = setTimeout(() => setDisplayedMessage(false), 2500);
  };
  useEffect(() => {
    // Clear the interval when the component unmounts
    return () => clearTimeout(timerRef.current);
  }, []);

  return (
    <View>
      {displayMessage ? (
        <View style={styles.messageContainer}>
          <Text style={styles.uploadSuccessfully}>
            This picture has been successfully uploaded
          </Text>
        </View>
      ) : null}

      <View style={{ alignItems: 'center' }}>
        {displayImage ? (
          <Animated.Image
            style={[styles.image, { opacity: fadeAnim }]}
            source={{ uri: props.imagePath }}
          />
        ) : null}

        <View style={styles.buttonRow}>
          <View style={{ marginHorizontal: 5 }}>
            <Button title="Show Image" onPress={showImage} />
          </View>

          {displayImage ? (
            <Button title="Close Image" onPress={closeImage} />
          ) : null}
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  messageContainer: {
    backgroundColor: 'rgba(4, 170, 109, .6)',
    marginBottom: 5,
    marginHorizontal: 13,
    borderRadius: 10,
    marginTop: 10,
  },
  uploadSuccessfully: {
    color: 'white',
    padding: 10,
    fontSize: 15,
  },
  image: {
    width: 250,
    height: 250,
    marginVertical: 10,
  },
  buttonRow: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    marginVertical: 16,
  },
});
export default SuccessfulUploaded;
