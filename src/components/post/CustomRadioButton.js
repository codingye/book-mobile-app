import { View, StyleSheet, Dimensions } from 'react-native';
import React from 'react';
import { ScreenDimensions } from '../../shared/constants';

import { CheckBox } from 'react-native-elements';

const CustomRadioButton = props => {
  return (
    <View style={styles.radioContainer}>
      <CheckBox
        {...props}
        containerStyle={styles.containerStyleCheck}
        textStyle={{ fontSize: 17 }}
        checkedIcon="dot-circle-o"
        uncheckedIcon="circle-o"
      />
    </View>
  );
};

const styles = StyleSheet.create({
  screen: {
    marginTop: 52,
    marginHorizontal: 8,
  },
  containerStyle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    flexWrap: 'wrap',
    paddingVertical: 15,
    borderWidth: 0.8,
    borderColor: '#0000001f',
    borderRadius: 10,
    shadowColor: 'black',
    shadowOpacity: 0.26,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 5,
    elevation: 5,
    backgroundColor: 'white',
  },
  labelContainer: {
    alignItems: 'center',
    flexDirection: 'row',
    marginLeft: 8,
    width: ScreenDimensions.WIDTH / 2,
    paddingVertical: 11,
  },
  radioContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  containerStyleCheck: {
    marginBottom: 10,
    width: ScreenDimensions.WIDTH / 2.5,
  },
  label: {
    fontSize: 18,
    color: '#00000099',
  },
  required: {
    color: '#c51244',
    marginLeft: 3,
    fontWeight: 'bold',
  },
});
export default CustomRadioButton;
