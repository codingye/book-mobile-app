import { View, Text, StyleSheet } from 'react-native';
import React from 'react';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { Colors } from '../../shared/constants';
const Card = props => {
  return (
    <View style={{ ...styles.container, ...props.containerStyle }}>
      <View style={styles.labelContainer}>
        <Icon name={props.iconName} size={30} color={Colors.COLOR_PRIMARY} />
        <Text style={styles.label}>{props.label}</Text>
      </View>
      <View style={{ ...styles.card, ...props.cardStyle }}>
        {props.children}
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    marginTop: 50,
    marginHorizontal: 10,
  },
  card: {
    marginTop: 16,
    shadowColor: 'black',
    shadowOpacity: 0.26,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 8,
    elevation: 5,
    borderRadius: 10,
    backgroundColor: 'white',
    borderWidth: 1,
  },
  labelContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  label: {
    marginLeft: 10,
    fontSize: 17,
    color: '#00000099',
    textTransform: 'uppercase',
  },
});
export default Card;
