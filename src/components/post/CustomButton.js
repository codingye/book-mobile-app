import { View, Text, StyleSheet, Button } from 'react-native';
import React from 'react';

const CustomButton = props => {
  return (
    <View style={{ ...styles.container, ...props.buttonStyleContainer }}>
      <View style={{ ...styles.button, ...props.buttonStyle }}>
        <Button {...props} title={props.title} color={props.buttonColor} />
      </View>
    </View>
  );
};

styles = StyleSheet.create({
  container: {
    marginHorizontal: 10,
  },
  button: {
    marginBottom: 15,
  },
});
export default CustomButton;
