import { View, Text, StyleSheet } from 'react-native';
import React, { useState } from 'react';
import { TextInput } from 'react-native-gesture-handler';
import { Colors } from '../../shared/constants';
import Message from './Message';
import { Icon } from 'react-native-elements';

const CustomInput = props => {
  const getErrorsColor = () => {
    let invalidStyle = {
      backgroundColor: '#fff0f4',
      borderColor: '#c51244',
      color: '#c51244',
    };
    let validStyle = {
      backgroundColor: '#ffffff',
      borderColor: '#0000001f',
      color: '#000000de',
    };
    if (props.error !== null) {
      return props.error ? invalidStyle : validStyle;
    }
    return props.error ? invalidStyle : validStyle;
  };
  let errorsStyle = getErrorsColor();
  return (
    <View style={{ ...styles.container, ...props.inputContainerStyle }}>
      <TextInput
        {...props}
        style={{
          ...styles.input,
          ...{
            borderColor: errorsStyle.borderColor,
            backgroundColor: errorsStyle.backgroundColor,
            color: errorsStyle.color,
          },
          ...props.inputStyle,
        }}
        placeholderTextColor="rgba(128,128,128,.4)"
      />
      {props.error ? <Message message={props.error} /> : null}
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    marginHorizontal: 10,
  },
  labelContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  label: {
    marginLeft: 10,
    fontSize: 18,
    color: '#00000099',
    textTransform: 'capitalize',
  },
  input: {
    height: 48,
    paddingLeft: 16,
    paddingEnd: 12,
    fontSize: 16,
    borderWidth: 1,
    borderColor: '#0000001f',
    borderRadius: 4,
    color: '#000000de',
  },
  required: {
    color: '#c51244',
    marginLeft: 3,
    fontWeight: 'bold',
  },
});
export default CustomInput;
