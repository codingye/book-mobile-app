import { View, Text, StyleSheet } from 'react-native';
import React from 'react';
import { Icon } from 'react-native-elements';
import { Colors, ScreenDimensions } from '../../shared/constants';
const Label = props => {
  return (
    <View style={{ ...styles.labelContainer, ...props.labelContainerStyle }}>
      <Icon
        raised
        size={19}
        name={props.iconName}
        type={props.iconType}
        color={Colors.COLOR_PRIMARY}
      />
      <Text style={styles.label}>{props.label}</Text>
      {props.requiredStar ? <Text style={styles.required}>*</Text> : null}
    </View>
  );
};

const styles = StyleSheet.create({
  labelContainer: {
    alignItems: 'center',
    flexDirection: 'row',
    marginLeft: 8,
    width: ScreenDimensions.WIDTH / 2,
    paddingVertical: 11,
    marginTop: 26,
  },
  label: {
    fontSize: 18,
    color: '#00000099',
    marginLeft: 6,
  },
  required: {
    color: '#c51244',
    marginLeft: 3,
    fontWeight: 'bold',
  },
});

export default Label;
