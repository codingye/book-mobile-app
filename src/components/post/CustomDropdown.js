import { View, Text, StyleSheet } from 'react-native';
import React, { useRef } from 'react';
import { Picker } from '@react-native-picker/picker';
import { Icon } from 'react-native-elements';
import { Colors } from '../../shared/constants';

const CustomDropdown = props => {
  let data = props.data;
  let itemList = data.map(item => (
    <Picker.Item
      style={{ color: 'black', backgroundColor: 'white' }}
      key={item.label}
      label={item.label}
      value={item.value}
    />
  ));
  const pickerRef = useRef();

  function open() {
    pickerRef.current.focus();
  }

  function close() {
    pickerRef.current.blur();
  }
  return (
    <View style={styles.dropdownContainer}>
      <View style={styles.dropdown}>
        <Picker {...props} ref={pickerRef} mode="dropdown">
          {itemList}
        </Picker>
      </View>
    </View>
  );
};
styles = StyleSheet.create({
  dropdownContainer: {
    marginHorizontal: 10,
    marginBottom: 10,
  },
  dropdown: {
    borderRadius: 8,
    shadowColor: 'black',
    shadowOffset: { width: 0, height: 2 },
    elevation: 5,
    shadowOpacity: 0.26,
    backgroundColor: 'white',
    borderColor: 'gray',
  },
  required: {
    color: '#c51244',
    marginLeft: 3,
    fontWeight: 'bold',
  },
});
export default CustomDropdown;
