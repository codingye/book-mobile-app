import { View, Text, StyleSheet } from 'react-native';
import { ScreenDimensions } from '../../shared/constants';
import React from 'react';

const Message = props => {
  return (
    <View style={{ ...styles.container, ...props.containerStyle }}>
      <View
        style={{ ...styles.messageContainer, ...props.messageContainerStyle }}
      >
        <Text style={styles.required}>*</Text>
        <Text style={{ ...styles.message, ...props.messageStyle }}>
          {props.message}
        </Text>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    marginStart: 12,
  },
  icon: {
    position: 'absolute',
    top: 1.4,
    left: 4,
  },
  messageContainer: {
    marginTop: 4,
    width: ScreenDimensions.WIDTH - 15,
    padding: 3,
    flexDirection: 'row',
  },
  message: {
    color: '#b91f1f',
    padding: 2,
    fontSize: 15,
    flexShrink: 1,
  },
  required: {
    color: '#c51244',
    marginStart: 3,
    fontWeight: 'bold',
    fontSize: 17,
  },
});
export default Message;
