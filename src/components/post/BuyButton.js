import {
  View,
  StyleSheet,
  Button,
  Text,
  ActivityIndicator,
} from 'react-native';
import React, { useState } from 'react';
import QRCode from 'react-native-qrcode-svg';
import { startTransaction } from '../../infrastructure/api/api';
import CustomButtonSecondary from '../custom-button/CustomButton';
import { Colors, ModalTexts } from '../../shared/constants';
import { useDispatch } from 'react-redux';
import LoadingSpinner from '../loading/LoadingSpinner';
import ModalCard from '../modal/Modal';

const BuyButton = props => {
  let { postId } = props;
  const [code, setCode] = useState('');
  const [transactionId, setTransactionId] = useState(null);
  const [modalShown, setModalShown] = useState(false);
  const [loading, setLoading] = useState(false);
  const dispatch = useDispatch();

  const getCode = async postId => {
    setLoading(true);
    try {
      let { data } = await startTransaction(postId);
      setCode(data.code);
      setTransactionId(data.id);
      setLoading(false);
      return data.code;
    } catch (e) {
      setLoading(false);
      dispatch(
        toggleModal(
          true,
          ModalTexts.EMPTY_FIELD_TITLE,
          ModalTexts.GENERAL_ERROR_DESCRIPTION,
        ),
      );
      setModalShown(true);
    }
  };

  const onButtonPress = () => {
    return getCode(postId);
  };

  return (
    <>
      <ModalCard
        modalShown={modalShown}
        onBackdropPress={() => setModalShown(false)}
        isBasicModal={true}
        cancelButton={ModalTexts.MODAL_OK}
      />
      {loading && (
        <ActivityIndicator size="large" color={Colors.COLOR_PRIMARY_GREEN} />
      )}
      <View
        style={{
          ...styles.container,
          ...props.buttonStyleContainer,
        }}
      >
        <View style={{ ...styles.button, ...props.buttonStyle }}>
          {!code && !loading && (
            <CustomButtonSecondary title="Get Now" onPress={onButtonPress} />
          )}
          {!!code && (
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                marginBottom: 20,
              }}
            >
              <Text
                style={{
                  fontSize: 18,
                  fontWeight: 'bold',
                  color: Colors.TEXT_GRAY,
                }}
              >
                Scan the QR code below
              </Text>
              <Text style={{ marginBottom: 12, color: Colors.TEXT_GRAY }}>
                The transaction will start automatically
              </Text>
              <View
                style={{
                  padding: 15,
                  borderColor: Colors.COLOR_GREEN_INACTIVE,
                  borderWidth: 2,
                  borderRadius: 10,
                }}
              >
                <QRCode
                  size={150}
                  value={JSON.stringify({ transactionId, code })}
                />
              </View>
            </View>
          )}
        </View>
      </View>
    </>
  );
};

styles = StyleSheet.create({
  container: {
    marginHorizontal: 10,
  },
  button: {
    marginBottom: 15,
  },
});

export default BuyButton;
