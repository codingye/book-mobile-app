import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import React from 'react';
import {
  Alert,
  PermissionsAndroid,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import { Colors, ScreenDimensions } from '../shared/constants';
import { Icon, Button } from 'react-native-elements';
import Message from './post/Message';
import SuccessfulUploaded from './post/SuccessfulUploaded';
import { useTranslation } from 'react-i18next';

export const ImagePicker = ({ setImage, imageData, errorMessage, error }) => {
  const { i18n, t } = useTranslation();

  const setLanguage = code => {
    return i18n.changeLanguage(code);
  };

  const onCamera = async () => {
    try {
      const granted = await askForPermissions();
      if (!granted) return;

      const result = await launchCamera({
        mediaType: 'photo',
        maxWidth: 800,
        maxHeight: 800,
      });

      if (result.didCancel) return;

      console.log('res', result);
      let { assets } = result;
      await setImage(assets[0]);
    } catch (e) {
      console.error(e);
    }
  };

  const onGallery = async () => {
    try {
      const result = await launchImageLibrary({
        maxWidth: 800,
        maxHeight: 800,
      });
      if (result.didCancel) return;
      let { assets } = result;
      await setImage(assets[0]);
    } catch (e) {
      console.error(e);
    }
  };

  const onCancelled = () => {
    return;
  };

  const askForPermissions = async () => {
    const permissions = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.CAMERA,
      {
        title: 'App Camera Permission',
        message: 'App needs access to your camera ',
        buttonNeutral: 'Ask Me Later',
        buttonNegative: 'Cancel',
        buttonPositive: 'OK',
      },
    );
    return permissions === PermissionsAndroid.RESULTS.GRANTED;
  };

  const onSelectImage = async () => {
    Alert.alert('Profile Picture', 'Choose an option', [
      { text: 'Camera', onPress: onCamera },
      { text: 'Gallery', onPress: onGallery },
      { text: 'Cancel', onPress: onCancelled },
    ]);
  };

  const getErrorsColor = () => {
    let invalidStyle = { backgroundColor: '#fff0f4', borderColor: '#c51244' };
    let validStyle = { backgroundColor: '#ffffff', borderColor: '#0000001f' };

    return error ? invalidStyle : validStyle;
  };

  let errorsStyle = getErrorsColor();

  return (
    <View style={styles.screen}>
      <View
        style={[
          styles.container,
          {
            borderColor: errorsStyle.borderColor,
            backgroundColor: errorsStyle.backgroundColor,
            color: errorsStyle.color,
          },
        ]}
      >
        <View style={styles.labelContainer}>
          <Icon
            raised
            name="md-cloud-upload-outline"
            type={'ionicon'}
            size={20}
            color={Colors.COLOR_PRIMARY}
          />
          <Text style={styles.label}>{t('common:uploade_picture_label')}</Text>
          <Text style={styles.required}>*</Text>
        </View>
        <View style={styles.containerButton}>
          <Button
            title={t('common:upload_button')}
            icon={{
              name: 'arrow-alt-circle-up',
              type: 'font-awesome-5',
              size: 15,
              color: 'white',
            }}
            iconContainerStyle={{ marginRight: 10 }}
            titleStyle={{ fontWeight: '700' }}
            buttonStyle={styles.button}
            containerStyle={styles.buttonContainer}
            onPress={onSelectImage}
          />
        </View>
      </View>
      {error ? (
        <Message message={errorMessage} />
      ) : imageData ? (
        <SuccessfulUploaded
          imagePath={
            imageData.constructor === Object ? imageData.uri : imageData
          }
        />
      ) : null}
    </View>
  );
};

const styles = StyleSheet.create({
  screen: {
    marginVertical: 20,
    marginTop: 62,
  },
  container: {
    flex: 1,
    flexDirection: 'row',
    borderWidth: 0.8,
    borderColor: '#0000001f',
    marginHorizontal: 8,
    shadowColor: '#00001f',
    shadowOpacity: 0.26,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 5,
    elevation: 5,
    borderRadius: 10,
    backgroundColor: '#ffffff',
  },
  labelContainer: {
    alignItems: 'center',
    flexDirection: 'row',
    marginLeft: 8,
    width: ScreenDimensions.WIDTH / 2,
    paddingVertical: 11,
  },
  label: {
    fontSize: 18,
    color: '#00000099',
  },
  required: {
    color: '#c51244',
    marginLeft: 3,
    fontWeight: 'bold',
  },
  containerButton: {
    marginLeft: 8,
    paddingVertical: 11,
    width: ScreenDimensions.WIDTH / 2,
  },
  buttonContainer: {
    marginVertical: 10,
    width: '72%',
    marginLeft: 7,
  },

  button: {
    backgroundColor: 'rgba(90, 154, 230, 1)',
    borderColor: 'transparent',
    borderWidth: 0,
    borderRadius: 30,
  },
});
