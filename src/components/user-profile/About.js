import { View, Text, StyleSheet } from 'react-native';
import React from 'react';
import { generalStyles, Colors } from '../../shared/constants';

const About = props => {
  return (
    <View style={styles.container}>
      <Text style={generalStyles.sectionTitle}>ABOUT ME</Text>
      <Text style={styles.about}>{props.about}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    margin: 32,
  },
  about: {
    fontSize: 15,
    fontWeight: '500',
    color: Colors.darkHL,
    marginTop: 8,
    lineHeight: 22,
  },
});

export default About;
