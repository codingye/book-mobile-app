import { View, Text, StyleSheet } from 'react-native';
import React from 'react';
import { generalStyles, Colors } from '../../shared/constants';
import EvilIcon from 'react-native-vector-icons/EvilIcons';
const Location = props => {
  return (
    <View style={styles.container}>
      <View style={{ width: 75, height: 75, justifyContent: 'center' }}>
        <EvilIcon name="location" size={60} color={Colors.orange} />
      </View>
      <View>
        <Text style={styles.location}>{props.City}</Text>
        <Text style={styles.distance}>{props.FullAddress}</Text>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    ...generalStyles.rowCenter,
    backgroundColor: Colors.lightBg,
    paddingHorizontal: 16,
    paddingVertical: 8,
  },
  location: {
    fontSize: 18,
    color: Colors.text,
    fontWeight: '500',
  },
  distance: {
    ...generalStyles.smallText,
    color: Colors.darkHL,
    marginTop: 4,
    textTransform: 'uppercase',
  },
});
export default Location;
