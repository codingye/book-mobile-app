import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native';
import React from 'react';
import LinearGradient from 'react-native-linear-gradient';
import { generalStyles, Colors } from '../../shared/constants';
import EntypoIcon from 'react-native-vector-icons/Entypo';
import AntIcon from 'react-native-vector-icons/AntDesign';

const Header = props => {
  return (
    <LinearGradient
      colors={[Colors.orange, Colors.COLOR_PRIMARY]}
      start={{ x: 0, y: 0 }}
      end={{ x: 1, y: 1 }}
    >
      <View style={styles.container}>
        <View style={styles.imageContainer}>
          <View>
            <View style={styles.check}>
              <AntIcon name="check" size={32} color={Colors.pink} />
            </View>
            <Image
              source={require('../../assets/photoUnsplash.jpg')}
              style={{ height: 100, width: 100, borderRadius: 32 }}
            />
          </View>
        </View>

        <View style={[generalStyles.center, { marginVertical: 12 }]}>
          <Text style={generalStyles.title}>{props.userName}</Text>
          <Text style={[generalStyles.subtitle, { marginTop: 8 }]}>
            {props.userEmail}
          </Text>
          <TouchableOpacity style={styles.editProfile}>
            <EntypoIcon
              name="feather"
              size={20}
              color="rgba(255, 255, 255, 0.6)"
            />
            <Text style={styles.editProfileText}>Edit Profile</Text>
          </TouchableOpacity>
        </View>
      </View>
    </LinearGradient>
  );
};
const styles = StyleSheet.create({
  container: {
    marginHorizontal: 32,
    paddingVertical: 64,
  },
  imageContainer: {
    ...generalStyles.center,
    marginTop: 16,
    shadowColor: Colors.darkBg,
    shadowOffset: { height: 3, width: 1 },
    shadowOpacity: 0.5,
  },
  check: {
    ...generalStyles.center,
    backgroundColor: Colors.text,
    borderRadius: 100,
    width: 32,
    height: 32,
    shadowColor: Colors.darkBg,
    shadowOffset: { height: 3, width: 1 },
    shadowOpacity: 0.3,
    position: 'absolute',
    zIndex: 1,
    right: -16,
    bottom: 16,
  },
  editProfile: {
    ...generalStyles.button,
    ...generalStyles.rowCenter,
    paddingHorizontal: 24,
    paddingVertical: 8,
    marginTop: 16,
    borderColor: 'rgba(255, 255, 255, 0.5)',
    borderWidth: 2,
    backgroundColor: Colors.COLOR_PRIMARY,
  },
  editProfileText: {
    fontSize: 16,
    color: Colors.text,
    fontWeight: '600',
    marginLeft: 4,
  },
});
export default Header;
