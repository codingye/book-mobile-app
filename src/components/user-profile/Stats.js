import {
  View,
  Text,
  TouchableOpacity,
  Linking,
  Platform,
  Alert,
  StyleSheet,
} from 'react-native';
import React from 'react';
import { generalStyles, Colors } from '../../shared/constants';
import MaterialIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import FeatherIcon from 'react-native-vector-icons/Feather';

const Stats = props => {
  const makeCall = () => {
    console.log('+++++++++callNumber ', props.Phone);
    let phoneNumber = props.Phone;
    if (Platform.OS == 'android') {
      phoneNumber = `tel:${props.Phone}`;
    }
    if (Platform.OS == 'ios') {
      phoneNumber = `telprompt:${props.Phone}`;
    }
    Linking.canOpenURL(phoneNumber)
      .then(supported => {
        if (!supported) {
          Alert.alert('Number is not available');
        } else {
          return Linking.openURL(phoneNumber);
        }
      })
      .catch(err => console.log(err));
  };

  const openWhatsApp = () => {
    let mobile = props.Phone;
    if (mobile) {
      let url = 'whatsapp://send?text=' + '&phone=20' + props.Phone;
      Linking.openURL(url)
        .then(data => {
          console.log('WhatsApp Opened successfully ' + data);
        })
        .catch(() => {
          alert('Make sure WhatsApp installed on your device');
        });
    } else {
      alert("there's no phone number");
    }
  };

  return (
    <View style={styles.container}>
      <View style={styles.statContainer}>
        <Text style={styles.statNumber}>{props.Gender}</Text>
        <View style={{ flexDirection: 'row' }}>
          <MaterialIcon name="gender-male" size={24} color={Colors.lightHL} />
          <Text style={styles.stat}>Gender</Text>
        </View>
      </View>

      <View style={[styles.statContainer, styles.divider]}>
        <Text style={styles.statNumber}>{props.Age}</Text>
        <View style={{ flexDirection: 'row' }}>
          <FeatherIcon name="user" size={24} color={Colors.lightHL} />
          <Text style={styles.stat}>Age</Text>
        </View>
      </View>

      <TouchableOpacity style={styles.statContainer} onPress={makeCall}>
        <Text style={[styles.statNumber, { fontSize: 16 }]}>{props.Phone}</Text>
        <View style={{ flexDirection: 'row' }}>
          <FeatherIcon name="phone" size={24} color={Colors.lightHL} />
          <Text style={styles.stat}>Phone</Text>
        </View>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    ...generalStyles.sectionContainer,
    ...generalStyles.rowBetween,
    marginHorizontal: 16,
    borderRadius: 16,
    marginTop: -48,
  },
  statContainer: {
    ...generalStyles.center,
    flex: 1,
  },
  statNumber: {
    fontSize: 20,
    fontWeight: '600',
    color: Colors.text,
  },
  stat: {
    fontSize: 11,
    fontWeight: '600',
    letterSpacing: 1,
    textTransform: 'uppercase',
    color: Colors.lightHL,
    marginTop: 6,
    paddingLeft: 5,
  },
  divider: {
    borderLeftWidth: 1,
    borderRightWidth: 1,
    borderColor: Colors.darkHL,
  },
});

export default Stats;
