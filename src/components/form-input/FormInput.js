import { TextInput } from 'react-native';
import React from 'react';
import { Colors } from '../../shared/constants';

const FormInput = props => {
  return (
    <TextInput
      selectionColor={Colors.COLOR_PRIMARY_GREEN}
      secureTextEntry={props.secureEntry}
      autoCapitalize={props.noCapitalize ? 'none' : 'sentences'}
      onBlur={props.handleBlur}
      onChangeText={props.handleChange}
      autoCorrect={false}
      value={props.value}
      placeholder={props.placeholder}
      keyboardType={props.isNumber ? 'decimal-pad' : 'default'}
      placeholderTextColor = {Colors.TEXT_GRAY}
      style={{
        height: 50,
        backgroundColor: Colors.COLOR_SECONDARY_GREEN,
        borderRadius: 10,
        fontSize: 18,
        fontWeight: '600',
        padding: 10,
        paddingStart: 50,
        width: '100%',
        color: Colors.COLOR_PRIMARY_GREEN,
      }}
    />
  );
};

export default FormInput;
