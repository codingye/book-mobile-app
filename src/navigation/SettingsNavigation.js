import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import SettingsHomeScreen from '../screens/settings/SettingsHomeScreen';
import UserProfile from '../screens/user-profile/UserProfile';
import { Colors } from '../shared/constants';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { TouchableOpacity } from 'react-native';
import ScanQRScreen from '../screens/settings/ScanQRScreen';
import HistoryScreen from '../screens/settings/HistoryScreen';
import MyPostDetailsScreen from '../screens/posts/MyPostDetailsScreen';
import { useTranslation } from 'react-i18next';

import LeaderboardScreen from '../screens/settings/LeaderboardScreen';
const Stack = createStackNavigator();

const SettingsNavigation = () => {
  const { i18n, t } = useTranslation();
  const setLanguage = code => {
    return i18n.changeLanguage(code);
  };

  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Setting"
        component={SettingsHomeScreen}
        options={() => ({
          headerShown: false,
        })}
      />
      <Stack.Screen
        name="UserProfile"
        component={UserProfile}
        options={({ navigation }) => ({
          headerTitle: t(`common:profile_label_settings_screen`),
          headerTitleAlign: 'center',
          headerStyle: {
            backgroundColor: Colors.COLOR_PRIMARY,
            elevation: 0,
            height: 70,
          },
          headerTitleStyle: {
            color: Colors.textLightGray,
            fontSize: 24,
            fontFamily: 'Poppins-Regular',
          },
          headerTintColor: Colors.textLightGray,
          headerLeft: () => (
            <TouchableOpacity
              style={{
                marginStart: 15,
                marginBottom: 3,
                backgroundColor: 'white',
                width: 50,
                height: 50,
                borderRadius: 25,
                justifyContent: 'center',
                alignItems: 'center',
                shadowColor: 'black',
                shadowOffset: { width: 0, height: 2 },
                elevation: 10,
                shadowOpacity: 0.26,
                paddingRight: 4,
              }}
              onPress={() => navigation.goBack()}
            >
              <Icon
                name="chevron-left"
                size={24}
                color={Colors.COLOR_PRIMARY}
              />
            </TouchableOpacity>
          ),
        })}
      />
      <Stack.Screen
        name="ScanQR"
        component={ScanQRScreen}
        options={({ navigation }) => ({
          headerTitle: t(`common:scan_code_qr_header`),
          headerTitleAlign: 'center',
          headerStyle: {
            backgroundColor: Colors.COLOR_PRIMARY,
            elevation: 0,
            height: 70,
          },
          headerTitleStyle: {
            color: Colors.textLightGray,
            fontSize: 24,
            fontFamily: 'Poppins-Regular',
          },
          headerTintColor: Colors.textLightGray,
          headerLeft: () => (
            <TouchableOpacity
              style={{
                marginStart: 15,
                marginBottom: 3,
                backgroundColor: 'white',
                width: 50,
                height: 50,
                borderRadius: 25,
                justifyContent: 'center',
                alignItems: 'center',
                shadowColor: 'black',
                shadowOffset: { width: 0, height: 2 },
                elevation: 10,
                shadowOpacity: 0.26,
                paddingRight: 4,
              }}
              onPress={() => navigation.goBack()}
            >
              <Icon
                name="chevron-left"
                size={24}
                color={Colors.COLOR_PRIMARY}
              />
            </TouchableOpacity>
          ),
        })}
      />
      <Stack.Screen
        name="Leaderboard"
        component={LeaderboardScreen}
        options={({ navigation }) => ({
          headerTitle: t(`common:leaderboard_label`),
          headerTitleAlign: 'center',
          headerStyle: {
            backgroundColor: Colors.COLOR_PRIMARY,
            elevation: 0,
            height: 70,
          },
          headerTitleStyle: {
            color: Colors.textLightGray,
            fontSize: 24,
            fontFamily: 'Poppins-Regular',
          },
          headerTintColor: Colors.textLightGray,
          headerLeft: () => (
            <TouchableOpacity
              style={{
                marginStart: 15,
                marginBottom: 3,
                backgroundColor: 'white',
                width: 50,
                height: 50,
                borderRadius: 25,
                justifyContent: 'center',
                alignItems: 'center',
                shadowColor: 'black',
                shadowOffset: { width: 0, height: 2 },
                elevation: 10,
                shadowOpacity: 0.26,
                paddingRight: 4,
              }}
              onPress={() => navigation.goBack()}
            >
              <Icon
                name="chevron-left"
                size={24}
                color={Colors.COLOR_PRIMARY}
              />
            </TouchableOpacity>
          ),
        })}
      />
      <Stack.Screen
        name="History"
        component={HistoryScreen}
        options={({ navigation }) => ({
          headerTitle: t(`common:all_transactions_header`),
          headerTitleAlign: 'center',
          headerStyle: {
            backgroundColor: Colors.COLOR_PRIMARY,
            elevation: 0,
            height: 70,
          },
          headerTitleStyle: {
            color: Colors.textLightGray,
            fontSize: 20,
            fontFamily: 'Poppins-Regular',
          },
          headerTintColor: Colors.textLightGray,
          headerLeft: () => (
            <TouchableOpacity
              style={{
                marginStart: 15,
                marginBottom: 3,
                backgroundColor: 'white',
                width: 50,
                height: 50,
                borderRadius: 25,
                justifyContent: 'center',
                alignItems: 'center',
                shadowColor: 'black',
                shadowOffset: { width: 0, height: 2 },
                elevation: 10,
                shadowOpacity: 0.26,
                paddingRight: 4,
              }}
              onPress={() => navigation.goBack()}
            >
              <Icon
                name="chevron-left"
                size={24}
                color={Colors.COLOR_PRIMARY}
              />
            </TouchableOpacity>
          ),
        })}
      />
      <Stack.Screen
        name="MyPostDetailsScreen"
        component={MyPostDetailsScreen}
        options={({ navigation }) => ({
          headerTransparent: true,
          headerStyle: { height: 70 },
          headerTitle: '',
          headerLeft: () => (
            <TouchableOpacity
              style={{
                marginStart: 15,
                marginBottom: 3,
                backgroundColor: 'white',
                width: 50,
                height: 50,
                borderRadius: 25,
                justifyContent: 'center',
                alignItems: 'center',
                shadowColor: 'black',
                shadowOffset: { width: 0, height: 2 },
                elevation: 10,
                shadowOpacity: 0.26,
                paddingRight: 4,
              }}
              onPress={() => navigation.goBack()}
            >
              <Icon
                name="chevron-left"
                size={30}
                color={Colors.COLOR_PRIMARY}
              />
            </TouchableOpacity>
          ),
        })}
      />
    </Stack.Navigator>
  );
};

export default SettingsNavigation;
