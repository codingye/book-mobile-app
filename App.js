import 'react-native-gesture-handler';
import React, { useEffect, useState } from 'react';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { Provider, useDispatch } from 'react-redux';
import { I18nManager, LogBox } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import store from './src/store';
import {
  notificationListener,
  requestUserPermission,
} from './src/infrastructure/api/services/NotificationServices';
import Splash from './src/screens/splash/Splash';
import messaging from '@react-native-firebase/messaging';
import { toggleNotification } from './src/store/actions/notification';
import { updateUserPoints } from './src/store/actions/auth';
import { getUser } from './src/infrastructure/api/api';
import './src/infrastructure/DSCLocalize'

LogBox.ignoreLogs([
  "[react-native-gesture-handler] Seems like you're using an old API with gesture components, check out new Gestures system!",
]);

try {
  I18nManager.allowRTL(false);
} catch (e) {
  console.log(e);
}

//Create AppWrapper function in order to use dispatch function inside App function
const AppWrapper = () => {
  return (
    <Provider store={store}>
      <App />
    </Provider>
  );
};

const App = () => {
  const dispatch = useDispatch();

  const foregroundNotificationListener = () => {
    return messaging().onMessage(async remoteMessage => {
      if (remoteMessage.notification.title.includes('transaction')) {
        const {points} = (await getUser()).data
        dispatch(updateUserPoints(points))
      }
      console.log('remote message', remoteMessage);
      dispatch(toggleNotification());
    });
  };
  useEffect(() => {
    requestUserPermission();
    notificationListener();
    foregroundNotificationListener();
  }, []);

  return (
    <SafeAreaProvider>
      <NavigationContainer>
        <Splash />
      </NavigationContainer>
    </SafeAreaProvider>
  );
};

export default AppWrapper;
