module.exports = {
  root: true,
  extends: ['eslint:recommended', 'plugin:react/recommended'],
  parser: 'babel-eslint',
  rules: {
    'no-unused-vars': 'warn',
    'no-useless-catch': 'warn',
    'no-useless-escape': 'warn',
    'no-undef': 'off',
    'react/prop-types': 'off',
    'react/display-name': 'off',
    'react/no-unescaped-entities': 'off',
    'no-async-promise-executor': 'warn',
  },
};
